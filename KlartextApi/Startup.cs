﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace OtrohetsakutenApi
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        public string ContentRootPath { get; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            ContentRootPath = env.ContentRootPath;

            // Add globally available data
            AppDomain.CurrentDomain.SetData("ContentRootPath", env.ContentRootPath);
            AppDomain.CurrentDomain.SetData("WebRootPath", env.WebRootPath);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // configure DI for application services
            services.AddScoped<IConfig, Config>();
            services.AddCors();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddApiVersioning();
            services.AddRouting(x => x.LowercaseUrls = true);
            services.AddHttpContextAccessor();
            services.AddDirectoryBrowser();
            // Get app settings
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // Configure JWT authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.UTF8.GetBytes(appSettings.Jwt.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.Events = new JwtBearerEvents
                {
                    OnTokenValidated = context =>
                    {
                        try
                        {

                        }
                        catch (Exception ex)
                        {
                        }

                        return Task.CompletedTask;
                    }
                };
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ClockSkew = TimeSpan.Zero,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true
                };
            });

            // Register Swagger generator
            services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                c.DocInclusionPredicate((version, desc) =>
                {
                    if (!desc.TryGetMethodInfo(out MethodInfo methodInfo)) return false;
                    var versions = methodInfo.DeclaringType.GetCustomAttributes(true).OfType<ApiVersionAttribute>().SelectMany(attr => attr.Versions);
                    var maps = methodInfo.GetCustomAttributes(true).OfType<MapToApiVersionAttribute>().SelectMany(attr => attr.Versions).ToArray();
                    return versions.Any(v => $"v{v.ToString()}" == version) && (maps.Length == 0 || maps.Any(v => $"v{v.ToString()}" == version));
                });
                c.IncludeXmlComments(Path.Combine(ContentRootPath, "App_Data", "XmlDocument.xml"));
                c.SwaggerDoc("v1", new Info
                {
                    Description = $"This API provides functionality to communicate with {appSettings.Owner.PossessionName} system.<br /><br />API calls requiring authentication needs to pass authentication type and token in the Authorization header. Example: Headers[\"Authorization\"] = \"Bearer AbC123\"<br />User language is set by using Accept-Language header. If Accept-Language header is not present and user language is empty it will be defaulted to {appSettings.Language.Default}. Accepted values: {string.Join(", ", appSettings.Language.Available)}<br />HTTP status 400 (Bad request) will return a list of <a href=\"#model-BadRequestError\">BadRequestError</a> objects.",
                    Title = $"{appSettings.Owner.Name} API",
                    Version = "v1"
                });
                SwaggerHelper.Filters(c);
            });

            //SignalR
            //services.AddSignalR();

        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var appSettingsSection = Configuration.GetSection("AppSettings");
            var appSettings = appSettingsSection.Get<AppSettings>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Add cors policy- Live
            app.UseCors(x => x
                .WithOrigins("https://admin.otrohetsakuten.se")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());


            // Add cors policy- Local
            //app.UseCors(x => x
            //    //For Live
            //    .WithOrigins("http://localhost:49915")
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()
            //    .AllowCredentials());

            // Add cors policy- Dev
            //app.UseCors(x => x
            //    .AllowAnyOrigin()
            //    .AllowAnyMethod()
            //    .AllowAnyHeader()
            //    .AllowCredentials());


            app.UseAuthentication();

            // Enable Swagger
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.DocumentTitle = $"{appSettings.Owner.Name} API";
                c.RoutePrefix = "docs";
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "API v1");
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseStaticFiles(); // For the wwwroot folder.

            // using Microsoft.Extensions.FileProviders;
            // using System.IO;
            app.UseFileServer(new FileServerOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(env.ContentRootPath, "Logs")),
                RequestPath = "/logs",
                EnableDirectoryBrowsing = true
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "default", template: "{controller}/{action=Index}/{id?}");
            });


        }
    }
}