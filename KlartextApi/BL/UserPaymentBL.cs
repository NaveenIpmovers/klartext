﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;


namespace OtrohetsakutenApi.BL
{
    public class UserPaymentBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;

        private string appDataPath = null;
        private string authCode = null;
        private string baseUrl = null;
        private object myLock = "lock";

        #endregion

        #region Constructor
        public UserPaymentBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }

        
        #endregion

        #region Public methods
        public UserPaymentInternal Add(UserPaymentInternal UserPayment)
        {
            UserPaymentInternal obj = null;

            try
            {
                if (UserPayment.Id == 0)
                {
                    List<SqlParameter> sqlParams = UserPayment.ToSqlParameters();
                    //Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.UserPayment} (userid, iapid, platform, receiptdata, purchasedate, expirydate) VALUES (@userid, @iapid, @platform, @receipt, @purchasedate, @expirydate);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        UserPayment.Id = Convert.ToInt64(id);
                        obj = Get(UserPayment.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public bool Update(UserPaymentInternal UserPayment)
        {
            bool updated = false;

            try
            {
                if (UserPayment.UserId > 0)
                {
                    List<SqlParameter> sqlParams = UserPayment.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.UserPayment} SET updatedate=GETUTCDATE(), iapid=@iapid, expirydate=@expirydate, platform=@platform, receiptdata=@receipt WHERE userid=@userid AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        public UserPaymentInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            UserPaymentInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<UserPaymentInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public UserPaymentInternal GetUserInfo(int UserId, bool Deleted = false, params string[] Columns)
        {
            UserPaymentInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@userid", Value = UserId });

                Paged<UserPaymentInternal> paged = List(0, 1, Columns, "userid=@userid", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }




        public Paged<UserPaymentInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<UserPaymentInternal>(api.TableNames.UserPayment, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        public bool Exists(long userid)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@userid", Value = userid });
                Paged<UserPaymentInternal> paged = List(0, 1, new string[] { "userid"}, "userid=@userid", sqlParams, "id DESC");

                if (paged != null)
                {
                    UserPaymentInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public bool DoPostForApple(string url, string postData, out string responseData)
        {
            bool success = false;

            try
            {
                // Configure request
                WebRequest request = WebRequest.Create(url);
                request.Method = "POST";
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = byteArray.Length;

                // Send data
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                // Get response
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                responseData = reader.ReadToEnd();

                // Close
                reader.Close();
                dataStream.Close();
                response.Close();

                // Set success
                success = true;
            }
            catch (Exception ex)
            {
                responseData = string.Empty;
            }

            return success;
        }
        public bool DoPostForGoogle(string url, out string responseData)
        {
            bool success = false;

            try
            {
                // Configure request
                WebRequest request = WebRequest.Create(url);
                request.Method = "GET";
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Get the stream containing content returned by the server. 
                // The using block ensures the stream is automatically closed. 
                using (Stream dataStream = response.GetResponseStream())
                {
                    // Open the stream using a StreamReader for easy access.  
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.  
                    responseData = reader.ReadToEnd();
                }

                // Close the response.  
                response.Close();

                // Set success
                success = true;
            }
            catch (Exception ex)
            {
                responseData = ex.ToString();
            }

            return success;
        }


        public void log(string message)
        {
            StreamWriter sw = null;

            try
            {
                if (message.Trim() != "")
                {
                    string storePath = config.Settings.Project.AppBaseUrl + "Log/";

                    // Write line
                    sw = new StreamWriter(storePath, true);
                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " > " + message);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                // Close
                if (sw != null)
                    sw.Close();
            }
        }
        #endregion

        #region In-App Purchase Android 

        /// <summary>
        /// Get Access Token From JSON Key Async
        /// </summary>
        /// <param name="jsonKeyFilePath">Path to your JSON Key file</param>
        /// <param name="scopes">Scopes required in access token</param>
        /// <returns>Access token as string Task</returns>
        public static async Task<string> GetAccessTokenFromJSONKeyAsync(string jsonKeyFilePath, params string[] scopes)
        {
            
            try
            {
                using (var stream = new FileStream(jsonKeyFilePath, FileMode.Open, FileAccess.Read))
                {

                    // Gets the credentials
                    var gCredential = GoogleCredential
                        .FromStream(stream) // Loads key file
                        .CreateScoped(scopes) // Gathers scopes requested
                        .UnderlyingCredential;

                    return await
                        gCredential.GetAccessTokenForRequestAsync(); // Gets the Access Token

                }
            }
            catch (Exception ex)
            {

              
            }
            return null;
        }

        /// <summary>
        /// Get Access Token From JSON Key
        /// </summary>
        /// <param name="jsonKeyFilePath">Path to your JSON Key file</param>
        /// <param name="scopes">Scopes required in access token</param>
        /// <returns>Access token as string</returns>
        public string GetAccessTokenFromJSONKey(string jsonKeyFilePath, params string[] scopes)
        {
            return GetAccessTokenFromJSONKeyAsync(jsonKeyFilePath, scopes).Result;
        }

        #endregion
    }
}