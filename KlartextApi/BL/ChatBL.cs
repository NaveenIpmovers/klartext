﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{

    public class ChatBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChatBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public ChatInternal Add(ChatInternal Chat)
        {
            ChatInternal obj = null;

            try
            {
                if (Chat.Id == 0)
                {
                    List<SqlParameter> sqlParams = Chat.ToSqlParameters();

                    // Add object
                    object id;

                    string sql = $"INSERT INTO {api.TableNames.Chat} (userid) VALUES (@userid);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        Chat.Id = Convert.ToInt32(id);
                        obj = Get(Chat.Id);
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
   
        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });
                // Delete object
                string sql = $"DELETE FROM {api.TableNames.ChatMessage} WHERE  createdate < DATEADD(hh, -24, GETDATE()) AND Chatid = {Id}";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public ChatInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            ChatInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChatInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public ChatInternal GetChatDetails(long Id, bool Deleted = false, params string[] Columns)
        {
            ChatInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@userid", Value = Id });

                Paged<ChatInternal> paged = List(0, 1, Columns, "userid=@userid", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<ChatInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<ChatInternal>(api.TableNames.Chat, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        #endregion
    }
}
