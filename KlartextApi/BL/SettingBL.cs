﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class SettingBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public SettingBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public Setting Add(Setting Setting)
        {
            Setting obj = null;

            try
            {
                if (Setting.Id ==0)
                {
                    List<SqlParameter> sqlParams = Setting.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"DECLARE @uniqueid UNIQUEIDENTIFIER;SET @uniqueid = NEWID();INSERT INTO {api.TableNames.Setting} (id, accesstoken) VALUES (@uniqueid, @accesstoken);SELECT @uniqueid;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                        obj = Get((Int32)id);
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }


        public Setting Get(int Id, bool Deleted = false, params string[] Columns)
        {
            Setting obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<Setting> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Setting GetAccessToken()
        {
            Setting obj = null;

            try
            {
                Paged<Setting> paged = List(0, 1, null, "", null, "id DESC");
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<Setting> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";
            return api.GetList<Setting>(api.TableNames.Setting, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy);
        }

        public bool UpdateAccessToken(string AccessToken)
        {
            bool updated = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@accesstoken", Value = AccessToken });

                // Update object
                string sql = $"UPDATE {api.TableNames.Setting} SET updatedate=GETUTCDATE(), accesstoken=@accesstoken WHERE deletedate IS NULL";
                updated = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}