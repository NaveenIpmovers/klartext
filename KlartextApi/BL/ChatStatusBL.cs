﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{

    public class ChatStatusBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChatStatusBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods

        public ChatStatusInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            ChatStatusInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChatStatusInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }


        public Paged<ChatStatusInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<ChatStatusInternal>(api.TableNames.ChatStatus, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        public bool Update(ChatStatusUpdate chatstatus)
        {
            bool updated = false;

            try
            {
                if (chatstatus.Id > 0)
                {
                    List<SqlParameter> sqlParams = chatstatus.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.ChatStatus} SET updatedate=GETUTCDATE(), manualopen=@manualopen, textmessage_enUS=@textmessage_enUS, textmessage_svSE=@textmessage_svSE WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion

    }
}
