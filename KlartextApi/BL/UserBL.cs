﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class UserBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public UserBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public UserInternal Add(UserInternal User)
        {
            UserInternal obj = null;

            try
            {
                if (User.Id == 0)
                {
                    List<SqlParameter> sqlParams = User.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.Users} (email, password, firstname, lastname, username, type, language, emailverified) VALUES (@email, @password, @firstname, @lastname, @username, @type, @language, @emailverified);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        User.Id = Convert.ToInt64(id);
                        obj = Get(User.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }

        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"UPDATE {api.TableNames.Users} SET deletedate=GETUTCDATE() WHERE id=@id AND deletedate IS NULL";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }

        public bool Exists(long Id)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<UserInternal> paged = List(0, 1, new string[] { "id" }, "id=@id", sqlParams, "id DESC");

                if (paged != null)
                {
                    UserInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public bool Exists(string Value, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });

                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<UserInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    UserInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public UserInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            UserInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<UserInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public UserInternal Get(string Email, bool Deleted = false, params string[] Columns)
        {
            UserInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@email", Value = Email });

                Paged<UserInternal> paged = List(0, 1, Columns, "email=@email", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public UserInternal Get(UserAuth UserAuth, bool Deleted = false, params string[] Columns)
        {
            UserInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = UserAuth.ToSqlParameters();

                Paged<UserInternal> paged = List(0, 1, Columns, "email=@email AND password=@password COLLATE Latin1_General_CS_AS", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<UserInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<UserInternal>(api.TableNames.Users, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        public bool Update(UserInternal User)
        {
            bool updated = false;

            try
            {
                if (User.Id > 0)
                {
                    List<SqlParameter> sqlParams = User.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.Users} SET updatedate=GETUTCDATE(), email=@email, password=@password, type=@type, firstname=@firstname, lastname=@lastname, username=@username, language=@language WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        public bool UpdateAppUser(UserInternal User)
        {
            bool updated = false;

            try
            {
                if (User.Id > 0)
                {
                    List<SqlParameter> sqlParams = User.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.Users} SET updatedate=GETUTCDATE(), password=@password, username=@username, language=@language WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}