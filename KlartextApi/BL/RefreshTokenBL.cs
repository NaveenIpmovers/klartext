﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class RefreshTokenBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public RefreshTokenBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public RefreshToken Add(RefreshToken RefreshToken)
        {
            RefreshToken obj = null;

            try
            {
                if (RefreshToken.Id == Guid.Empty)
                {
                    List<SqlParameter> sqlParams = RefreshToken.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"DECLARE @uniqueid UNIQUEIDENTIFIER;SET @uniqueid = NEWID();INSERT INTO {api.TableNames.RefreshTokens} (id, userid, token, accesstoken) VALUES (@uniqueid, @userid, @token, @accesstoken);SELECT @uniqueid;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                        obj = Get((Guid)id);
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }

        public bool Delete(Guid Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Guid, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"DELETE {api.TableNames.RefreshTokens} WHERE id=@id";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }

        public RefreshToken Get(Guid Id, bool Deleted = false, params string[] Columns)
        {
            RefreshToken obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Guid, ParameterName = "@id", Value = Id });

                Paged<RefreshToken> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public RefreshToken Get(string Token, bool Deleted = false, params string[] Columns)
        {
            RefreshToken obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@token", Value = Token });

                Paged<RefreshToken> paged = List(0, 1, Columns, "token=@token COLLATE Latin1_General_CS_AS", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public RefreshToken GetByAccessToken(string AccessToken, bool Deleted = false, params string[] Columns)
        {
            RefreshToken obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@token", Value = AccessToken });

                Paged<RefreshToken> paged = List(0, 1, Columns, "accesstoken=@token COLLATE Latin1_General_CS_AS", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<RefreshToken> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";
            return api.GetList<RefreshToken>(api.TableNames.RefreshTokens, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy);
        }

        public bool UpdateDate(long UserId, string AccessToken)
        {
            bool updated = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@accesstoken", Value = AccessToken });
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@userid", Value = UserId });

                // Update object
                string sql = $"UPDATE {api.TableNames.RefreshTokens} SET updatedate=GETUTCDATE() WHERE deletedate IS NULL AND userid=@userid AND accesstoken=@accesstoken COLLATE Latin1_General_CS_AS";
                updated = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}