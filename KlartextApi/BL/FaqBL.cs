﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{
   
    public class FaqBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public FaqBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public FaqInternal Add(FaqInternal Faq)
        {
            FaqInternal obj = null;

            try
            {
                if (Faq.Id == 0)
                {
                    List<SqlParameter> sqlParams = Faq.ToSqlParameters();
                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.Faq} (categoryid, question_enus, question_svse, answer_enus, answer_svse, status, sortorder) VALUES (@categoryid, @question_enus,  @question_svse, @answer_enus, @answer_svse, @status, @sortorder);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        Faq.Id = Convert.ToInt32(id);
                        obj = Get(Faq.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"UPDATE {api.TableNames.Faq} SET deletedate=GETUTCDATE() WHERE id=@id AND deletedate IS NULL";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public FaqInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            FaqInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<FaqInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public FaqInternal Get(string Title, bool Deleted = false, params string[] Columns)
        {
            FaqInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@title", Value = Title });

                Paged<FaqInternal> paged = List(0, 1, Columns, "title=@title", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public bool Exists(string Value, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });

                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<FaqInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    FaqInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }
        public Paged<FaqInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"Status= 1" + " AND " + $"deletedate IS {deleted}";

            return api.GetList<FaqInternal>(api.TableNames.Faq, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        public bool Update(FaqInternal faq)
        {
            bool updated = false;

            try
            {
                if (faq.Id > 0)
                {
                    List<SqlParameter> sqlParams = faq.ToSqlParameters();
                    // Update object
                    string sql = $"UPDATE {api.TableNames.Faq} SET updatedate=GETUTCDATE(), question_enus=@question_enus, question_svse=@question_svse, answer_enus=@answer_enus,  answer_svse=@answer_svse, status=@status, sortorder=@sortorder WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}
