﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace OtrohetsakutenApi.BL
{

    public class ChapterPartDataBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChapterPartDataBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods

        public ChapterPartDataInternal Get(int Id, bool Deleted = false, params string[] Columns)
        {
            ChapterPartDataInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChapterPartDataInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC");
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public ChapterPartDataInternal Get(int Id, string ModuleGuid, bool Deleted = false, params string[] Columns)
        {
            ChapterPartDataInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@chapterpartid", Value = Id });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@moduleguid", Value = ModuleGuid });

                Paged<ChapterPartDataInternal> paged = List(0, 1, Columns, "chapterpartid= @chapterpartid AND moduleguid=@moduleguid", sqlParams, "id DESC");
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public ChapterPartDataInternal Add(ChapterPartDataInternal ChapterPartData, long ChapterPartId)
        {
            ChapterPartDataInternal obj = null;

            try
            {
                if (ChapterPartData.Id == 0)
                {
                    List<SqlParameter> sqlParams = ChapterPartData.ToSqlParameters();
                    sqlParams.Add(new SqlParameter("@ChapterPartId", ChapterPartId));
                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.ChapterPartData} (chapterpartid, moduletypeid,title_enus, title_svse, subtitle_enus,subtitle_svse, description_enus, description_svse, optiontext_enus, optiontext_svse, moduleguid, sortorder, imageurl, useimage) VALUES (@chapterpartid, @moduletypeid, @title_enus, @title_svse, @subtitle_enus,  @subtitle_svse, @description_enus,@description_svse, @optiontext_enus, @optiontext_svse, @moduleguid, @sortorder, @imageurl, @useimage);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        ChapterPartData.Id = Convert.ToInt32(id);
                        obj = Get(ChapterPartData.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        /// <summary>
        /// If user delete multiple records from list of record
        /// </summary>
        /// <param name="chapterPartId"></param>
        /// <param name="Ids"></param>
        /// <param name="moduleGuid"></param>
        /// <returns></returns>
        public bool SingleDelete(int chapterPartId, string Ids, Guid moduleGuid)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();

                // Delete object (Mark delete date in Chapter Part data)
                string sql = $"UPDATE {api.TableNames.ChapterPartData} set deletedate = GETUTCDATE() WHERE Id NOT IN ({Ids}) AND ChapterPartID={chapterPartId} AND Moduleguid='{moduleGuid}'";

                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public bool Delete(int Id, string ModuleGuid)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@ChapterPartId", Value = Id });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@ModuleGuid", Value = ModuleGuid });

                // Delete object (Mark delete date in Chapter Part data and User data)
                string sql = $"UPDATE {api.TableNames.ChapterPartData} set deletedate = GETUTCDATE() WHERE ChapterPartId=@ChapterPartId AND ModuleGuid=@ModuleGuid AND deletedate IS NULL;" +
                                 $"UPDATE {api.TableNames.UserData} set deletedate = GETUTCDATE() where ChapterPartId=@ChapterPartId AND ModuleGuid = @ModuleGuid";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public bool DeleteAudio(int Id, int AudioType, string ModuleGuid)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@ChapterPartId", Value = Id });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@ModuleGuid", Value = ModuleGuid });
                string sql = string.Empty;
                // Update object (Mark audio url english to blank)
                if (Id > 0 && AudioType == 1)
                {

                    sql = $"UPDATE { api.TableNames.ChapterPartData} set audiourl_enus = '' where  ChapterPartId=@ChapterPartId AND ModuleGuid = @ModuleGuid";
                }
                // Update object (Mark audio url english to blank)
                if (Id > 0 && AudioType == 2)
                {
                    sql = $"UPDATE { api.TableNames.ChapterPartData} set audiourl_svse = '' where  ChapterPartId=@ChapterPartId AND ModuleGuid = @ModuleGuid";
                }
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public bool Update(ChapterPartDataInternal chapterpartdata)
        {
            bool updated = false;

            try
            {
                if (chapterpartdata.Id > 0)
                {
                    List<SqlParameter> sqlParams = chapterpartdata.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.ChapterPartData} SET updatedate=GETUTCDATE(), title_enus=@title_enus, title_svse=@title_svse, subtitle_enus=@subtitle_enus,  subtitle_svse=@subtitle_svse, description_enus=@description_enus, description_svse=@description_svse, optiontext_enus=@optiontext_enus, optiontext_svse=@optiontext_svse, sortorder=@sortorder, imageurl=@imageurl, useimage=@useimage WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        public bool UpdateAudio(string audioUrl_enUS, string audioUrl_svSE, string moduleguid, int chapterPartId)
        {
            bool updated = false;

            try
            {
                if (!string.IsNullOrWhiteSpace(audioUrl_enUS))
                {
                    // Update object
                    string sql = $"UPDATE {api.TableNames.ChapterPartData} SET updatedate=GETUTCDATE(), audiourl_enUS='{"english/" + audioUrl_enUS}' WHERE moduleguid='{moduleguid}' AND chapterpartid={chapterPartId}  AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql);
                }
                if (!string.IsNullOrWhiteSpace(audioUrl_svSE))
                {
                    // Update object
                    string sql = $"UPDATE {api.TableNames.ChapterPartData} SET updatedate=GETUTCDATE(), audiourl_svSE='{"swedish/" + audioUrl_svSE}' WHERE moduleguid='{moduleguid}' AND chapterpartid={chapterPartId}  AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql);
                }

            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        public Paged<ChapterPartDataInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<ChapterPartDataInternal>(api.TableNames.ChapterPartData, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        #endregion
    }
}
