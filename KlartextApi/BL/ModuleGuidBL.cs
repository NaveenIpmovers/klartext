﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{
   
    public class ModuleGuidBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ModuleGuidBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public ModuleGroupGuidInternal Get(int Id, bool Deleted = false, params string[] Columns)
        {
            ModuleGroupGuidInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@chapterpartid", Value = Id });

                Paged<ModuleGroupGuidInternal> paged = List(0, 1, Columns, "chapterpartid=@chapterpartid AND moduletypeid in (2,4,5)", sqlParams, "id DESC");
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public Paged<ModuleGroupGuidInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            return api.GetList<ModuleGroupGuidInternal>(api.TableNames.ChapterPartData, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
    
        #endregion
    }
}
