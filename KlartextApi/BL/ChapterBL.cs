﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{
   
    public class ChapterBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChapterBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public ChapterInternal Add(ChapterInternal Chapter)
        {
            ChapterInternal obj = null;

            try
            {
                if (Chapter.Id == 0)
                {
                    List<SqlParameter> sqlParams = Chapter.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.Chapter} (overviewid, description_enus, description_svse, imageurl, status, sortorder) VALUES (@overviewid, @description_enus, @description_svse, @imageurl, @status, @sortorder);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        Chapter.Id = Convert.ToInt32(id);
                        obj = Get(Chapter.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });
                // Delete object
                string sql = $"UPDATE {api.TableNames.UserData} set deletedate = GETUTCDATE() where chapterpartID IN(select chapterpartID FROM {api.TableNames.ChapterPart} where ChapterID = @id);" +
                               $"UPDATE {api.TableNames.ChapterPartData} set deletedate = GETUTCDATE() where chapterpartID IN(select chapterID FROM {api.TableNames.ChapterPart} where ChapterID = @id);" +
                                 $"UPDATE {api.TableNames.ChapterPart} set deletedate = GETUTCDATE() where ChapterID = @id;" +
                                     $"UPDATE {api.TableNames.Chapter} set deletedate = GETUTCDATE() where ID = @id AND deletedate IS NULL";

                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public ChapterInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            ChapterInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChapterInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public ChapterInternal Get(string Title, bool Deleted = false, params string[] Columns)
        {
            ChapterInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@title", Value = Title });

                Paged<ChapterInternal> paged = List(0, 1, Columns, "title=@title", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public bool Exists(long overviewid, string description_enus, string description_svse)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@overviewid", Value = overviewid });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@description_enus", Value = description_enus });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@description_svse", Value = description_svse });
                Paged<ChapterInternal> paged = List(0, 1, new string[] { "overviewid", "description_enus", description_svse }, "overviewid=@overviewid AND description_enus=@description_enus AND description_svse=@description_svse", sqlParams, "id DESC");

                if (paged != null)
                {
                    ChapterInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public bool Exists(string Value, string Value1, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value1 });
                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<ChapterInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    ChapterInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }
        public Paged<ChapterInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"Status= 1" + " AND " + $"deletedate IS {deleted}";

            return api.GetList<ChapterInternal>(api.TableNames.Chapter, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        public bool Update(ChapterInternal chapter)
        {
            bool updated = false;

            try
            {
                if (chapter.Id > 0)
                {
                    List<SqlParameter> sqlParams = chapter.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.Chapter} SET updatedate=GETUTCDATE(), description_enus=@description_enus, description_svse=@description_svse, status=@status, sortorder=@sortorder, imageurl=@imageurl WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}
