﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace OtrohetsakutenApi.BL
{
    public class NotificationQueueBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public NotificationQueueBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public NotificationQueueInternal Add(NotificationQueueInternal NotificationQueue)
        {
            NotificationQueueInternal obj = null;

            try
            {
                if (NotificationQueue.Id == 0)
                {
                    List<SqlParameter> sqlParams = NotificationQueue.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.NotificationQueue} (title_enus, title_svse, message_enus, message_svse) VALUES (@title_enus, @title_svse, @message_enus, @message_svse);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        NotificationQueue.Id = Convert.ToInt64(id);
                        obj = Get(NotificationQueue.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }

        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"UPDATE {api.TableNames.NotificationQueue} SET deletedate=GETUTCDATE() WHERE id=@id AND deletedate IS NULL";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }

        public bool Exists(long Id)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<NotificationQueueInternal> paged = List(0, 1, new string[] { "id" }, "id=@id", sqlParams, "id DESC");

                if (paged != null)
                {
                    NotificationQueueInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public bool Exists(string Value, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });

                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<NotificationQueueInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    NotificationQueueInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public NotificationQueueInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            NotificationQueueInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<NotificationQueueInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }


        public Paged<NotificationQueueInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<NotificationQueueInternal>(api.TableNames.NotificationQueue, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        public bool Update(NotificationQueueInternal News)
        {
            bool updated = false;

            try
            {
                if (News.Id > 0)
                {
                    List<SqlParameter> sqlParams = News.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.NotificationQueue} SET updatedate=GETUTCDATE(), title_enus=@title_enus, title_svse=@title_svse, message_enus=@message_enus, message_svse=@message_svse WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}