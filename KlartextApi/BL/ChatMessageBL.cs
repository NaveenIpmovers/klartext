﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{

    public class ChatMessageBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChatMessageBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public ChatMessageInternal Add(ChatMessageInternal ChatMessage)
        {
            ChatMessageInternal obj = null;

            try
            {
                if (ChatMessage.Id == 0)
                {
                    List<SqlParameter> sqlParams = ChatMessage.ToSqlParameters();

                    // Add object
                    object id;

                    string sql = $"INSERT INTO {api.TableNames.ChatMessage} (chatid, userid, text) VALUES (@chatid, @userid, @text);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        ChatMessage.Id = Convert.ToInt32(id);
                        obj = Get(ChatMessage.Id);
                    }

                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public ChatMessageInternal AddMessage(ChatMessageInternal Chat)
        {
            ChatMessageInternal obj = null;

            try
            {
                if (Chat.Id == 0)
                {
                    List<SqlParameter> sqlParams = Chat.ToSqlParameters();

                    // Add object
                    object id;

                    string sql = $"INSERT INTO {api.TableNames.ChatMessage} (chatid, userid, text) VALUES (@id, @userid, @text);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);
                    if (added && id != null)
                    {
                        Chat.Id = Convert.ToInt32(id);
                        obj = Get(Chat.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
   
        public ChatMessageInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            ChatMessageInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChatMessageInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<ChatMessageInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<ChatMessageInternal>(api.TableNames.ChatMessage, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        public Paged<ChatMessageInternal> ListOldMessages(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<ChatMessageInternal>(api.TableNames.ChatMessage, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        public ChatMessageInternal ifChatMessageExist(long Id, bool Deleted = false, params string[] Columns)
        {
            ChatMessageInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<ChatMessageInternal> paged = List(0, 1, Columns, "chatid=@id", sqlParams, "chatid DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        #endregion



    }
}
