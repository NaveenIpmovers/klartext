﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{
   
    public class CategoryBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public CategoryBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public CategoryInternal Add(CategoryInternal Category)
        {
            CategoryInternal obj = null;

            try
            {
                if (Category.Id == 0)
                {
                    List<SqlParameter> sqlParams = Category.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.Category} (name_enus, name_svse) VALUES (@name_enus,@name_svse);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        Category.Id = Convert.ToInt32(id);
                        obj = Get(Category.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"UPDATE {api.TableNames.Category} SET deletedate=GETUTCDATE() WHERE id=@id AND deletedate IS NULL";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public CategoryInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            CategoryInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@id", Value = Id });

                Paged<CategoryInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public CategoryInternal Get(string Title, bool Deleted = false, params string[] Columns)
        {
            CategoryInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@title", Value = Title });

                Paged<CategoryInternal> paged = List(0, 1, Columns, "title=@title", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }
        public bool Exists(string Value, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });

                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<CategoryInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    CategoryInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }
        public Paged<CategoryInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");

            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "")  + $"deletedate IS {deleted}";

            return api.GetList<CategoryInternal>(api.TableNames.Category, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        public bool Update(CategoryInternal faq)
        {
            bool updated = false;

            try
            {
                if (faq.Id > 0)
                {
                    List<SqlParameter> sqlParams = faq.ToSqlParameters();
                    // Update object
                    string sql = $"UPDATE {api.TableNames.Category} SET updatedate=GETUTCDATE(), name_enus=@name_enus, name_svse=@name_svse WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}
