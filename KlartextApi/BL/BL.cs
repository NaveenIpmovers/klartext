﻿using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class BL
    {
        #region Variables
        private IConfig config = null;
        private RefreshTokenBL refreshToken = null;
        private UserBL user = null;
        private ChapterBL chapter = null;
        private ChapterPartBL chapterpart = null;
        private ChapterPartDataBL chapterpartdata = null;
        private UserDataBL userdata = null;
        private ModuleBL module = null;
        private ModuleGuidBL moduleguid = null;
        private FaqBL faq = null;
        private OverviewBL overview = null;
        private UserPaymentBL userpayment = null;
        private SettingBL setting = null;
        private ChatBL chat = null;
        private ChatMessageBL chatmessage = null;
        private ChatStatusBL chatstatus = null;
        private CategoryBL category = null;
        private NotificationBL notification = null;
        private NotificationQueueBL notificationqueue = null;
        
        #endregion

        #region Properties
        public RefreshTokenBL RefreshToken
        {
            get
            {
                if (refreshToken == null)
                    refreshToken = new RefreshTokenBL(config);
                return refreshToken;
            }
        }

        public UserBL User
        {
            get
            {
                if (user == null)
                    user = new UserBL(config);
                return user;
            }
        }

        public ChapterBL Chapter
        {
            get
            {
                if (chapter == null)
                    chapter = new ChapterBL(config);
                return chapter;
            }
        }
        public ChapterPartBL ChapterPart
        {
            get
            {
                if (chapterpart == null)
                    chapterpart = new ChapterPartBL(config);
                return chapterpart;
            }
        }
        public ChapterPartDataBL ChapterPartData
        {
            get
            {
                if (chapterpartdata == null)
                    chapterpartdata = new ChapterPartDataBL(config);
                return chapterpartdata;
            }
        }

        public UserDataBL UserData
        {
            get
            {
                if (userdata == null)
                    userdata = new UserDataBL(config);
                return userdata;
            }
        }
        public ModuleBL Module
        {
            get
            {
                if (module == null)
                    module = new ModuleBL(config);
                return module;
            }
        }
        public ModuleGuidBL ModuleGuid
        {
            get
            {
                if (moduleguid == null)
                    moduleguid = new ModuleGuidBL(config);
                return moduleguid;
            }
        }

        public FaqBL Faq
        {
            get
            {
                if (faq == null)
                    faq = new FaqBL(config);
                return faq;
            }
        }

        public OverviewBL Overview
        {
            get
            {
                if (overview == null)
                    overview = new OverviewBL(config);
                return overview;
            }
        }

        public UserPaymentBL UserPayment
        {
            get
            {
                if (userpayment == null)
                    userpayment = new UserPaymentBL(config);
                return userpayment;
            }
        }
        public SettingBL Setting
        {
            get
            {
                if (setting == null)
                    setting = new SettingBL(config);
                return setting;
            }
        }
        public ChatBL Chat
        {
            get
            {
                if (chat == null)
                    chat = new ChatBL(config);
                return chat;
            }
        }
        public ChatMessageBL ChatMessage
        {
            get
            {
                if (chatmessage == null)
                    chatmessage = new ChatMessageBL(config);
                return chatmessage;
            }
        }
        public ChatStatusBL ChatStatus
        {
            get
            {
                if (chatstatus == null)
                    chatstatus = new ChatStatusBL(config);
                return chatstatus;
            }
        }
        public CategoryBL Category
        {
            get
            {
                if (category == null)
                    category = new CategoryBL(config);
                return category;
            }
        }
        
        public NotificationBL Notification
        {
            get
            {
                if (notification == null)
                    notification = new NotificationBL(config);
                return notification;
            }
        }
        public NotificationQueueBL NotificationQueue
        {
            get
            {
                if (notificationqueue == null)
                    notificationqueue = new NotificationQueueBL(config);
                return notificationqueue;
            }
        }
        
        #endregion

        #region Constructor
        public BL(IConfig Config)
        {
            config = Config;
        }
        #endregion
    }
}