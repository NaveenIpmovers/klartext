﻿using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.BL
{

    public class ChapterPartBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public ChapterPartBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public ChapterPartInternal Add(ChapterPartInternal ChapterData)
        {
            ChapterPartInternal obj = null;

            try
            {
                if (ChapterData.Id == 0)
                {
                    List<SqlParameter> sqlParams = ChapterData.ToSqlParameters();
                   // sqlParams.Add(new SqlParameter("@ChapterId", ChapterId));
                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.ChapterPart} (chapterid, title_enUS, title_svSE, parttitle_enUS, parttitle_svSE, imageurl, ispaid) VALUES (@ChapterId, @title_enUS, @title_svSE, @parttitle_enUS, @parttitle_svSE,  @imageurl, @ispaid);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        ChapterData.Id = Convert.ToInt32(id);
                        obj = Get(ChapterData.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }
        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object from user data, chapterpartdata, chapterpart
                string sql = $"UPDATE {api.TableNames.UserData} set deletedate = GETUTCDATE() where chapterpartID =@id;" +
                               $"UPDATE {api.TableNames.ChapterPartData} set deletedate = GETUTCDATE() where chapterpartID =@id;" +
                                 $"UPDATE {api.TableNames.ChapterPart} set deletedate = GETUTCDATE() where Id = @id";

                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        public bool Update(ChapterPartInternal chapterpart)
        {
            bool updated = false;

            try
            {
                if (chapterpart.Id > 0)
                {
                    List<SqlParameter> sqlParams = chapterpart.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE {api.TableNames.ChapterPart} SET updatedate=GETUTCDATE(), title_enUS=@title_enUS, title_svSE=@title_svSE, parttitle_enUS=@parttitle_enUS, parttitle_svSE=@parttitle_svSE, imageurl=@imageurl, ispaid=@ispaid WHERE id=@id AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        public ChapterPartInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            ChapterPartInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<ChapterPartInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC",Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public ChapterPartInternal Get(string Title, bool Deleted = false, params string[] Columns)
        {
            ChapterPartInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@title", Value = Title });

                Paged<ChapterPartInternal> paged = List(0, 1, Columns, "title=@title", sqlParams, "programid DESC");
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<ChapterPartInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "")  + $"deletedate IS {deleted}";
            return api.GetList<ChapterPartInternal>(api.TableNames.ChapterPart, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }

        #endregion
    }
}
