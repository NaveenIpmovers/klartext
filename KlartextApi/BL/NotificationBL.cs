﻿using System;
using System.Net;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class NotificationBL
    {
        #region Variables
        private IConfig config = null;
        #endregion

        #region Constructor
        public NotificationBL(IConfig Config)
        {
            config = Config;
        }
        #endregion

        #region Public methods
        public bool Register(string UserId, MobileDevice MobileDevice)
        {
            bool added = false;

            try
            {
                // Set default
                if (MobileDevice.Platform == null)
                    MobileDevice.Platform = "";

                // Register device
                WebClient wc = new WebClient();
                string response = wc.DownloadString(string.Format("{0}register.ashx?key={1}&service={2}&userid={3}&deviceid={4}&platform={5}", config.Settings.Notification.BaseUrl, Uri.EscapeDataString(config.Settings.Notification.Key), Uri.EscapeDataString(MobileDevice.Service), Uri.EscapeDataString(UserId), Uri.EscapeDataString(MobileDevice.DeviceId), Uri.EscapeDataString(MobileDevice.Platform)));
                added = (response.ToLower() == "success");
            }
            catch (Exception ex)
            {
            }

            return added;
        }

        public bool Send(Notification Notification)
        {
            return (Send(new Notification[] { Notification }) == 1);
        }

        public int Send(params Notification[] Notifications)
        {
            int count = 0;

            foreach (Notification notification in Notifications)
            {
                try
                {
                    // Default values
                    if (string.IsNullOrWhiteSpace(notification.Message))
                        notification.Message = string.Empty;
                    if (string.IsNullOrWhiteSpace(notification.Title))
                        notification.Title = string.Empty;

                    // Set URL
                    string url = string.Format("{0}send.ashx?key={1}&userid={2}&title={3}&message={4}&badge={5}&priority={6}", config.Settings.Notification.BaseUrl, Uri.EscapeDataString(config.Settings.Notification.Key), notification.UserId, Uri.EscapeDataString(notification.Title), Uri.EscapeDataString(notification.Message), notification.Badge, (int)notification.Priority);
                    if (notification.Data != null)
                        foreach (var item in notification.Data)
                            url += string.Format("&data.{0}={1}", item.Key, Uri.EscapeDataString(item.Value));

                    // Send notification
                    WebClient wc = new WebClient();
                    string response = wc.DownloadString(url);
                    if (response.ToLower() == "success")
                        count++;
                }
                catch (Exception ex)
                {
                }
            }

            return count;
        }

        public bool Unregister(MobileDevice MobileDevice)
        {
            bool deleted = false;

            try
            {
                // Unregister device
                WebClient wc = new WebClient();
                string response = wc.DownloadString(string.Format("{0}unregister.ashx?key={1}&service={2}&deviceid={3}", config.Settings.Notification.BaseUrl, Uri.EscapeDataString(config.Settings.Notification.Key), Uri.EscapeDataString(MobileDevice.Service), Uri.EscapeDataString(MobileDevice.DeviceId)));
                deleted = (response.ToLower() == "success");
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }
        #endregion
    }
}