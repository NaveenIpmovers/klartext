﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.BL
{
    public class UserDataBL
    {
        #region Variables
        private ApiHelper api = null;
        private IConfig config = null;
        #endregion

        #region Constructor
        public UserDataBL(IConfig Config)
        {
            api = new ApiHelper(Config);
            config = Config;
        }
        #endregion

        #region Public methods
        public UserDataInternal Add(UserDataInternal UserData)
        {
            UserDataInternal obj = null;

            try
            {
                if (UserData.Id == 0)
                {
                    List<SqlParameter> sqlParams = UserData.ToSqlParameters();

                    // Add object
                    object id;
                    string sql = $"INSERT INTO {api.TableNames.UserData} (userid, chapterpartid, moduletypeid, moduleguid, data, done) VALUES (@userid, @chapterpartid, @moduletypeid, @moduleguid, @data, @done);SELECT @@IDENTITY;";
                    bool added = api.Database.ExecuteScalar(sql, sqlParams, out id);

                    if (added && id != null)
                    {
                        UserData.Id = Convert.ToInt64(id);
                        obj = Get(UserData.Id);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return obj;
        }

        public bool Delete(long Id)
        {
            bool deleted = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                // Delete object
                string sql = $"UPDATE {api.TableNames.Users} SET deletedate=GETUTCDATE() WHERE id=@id AND deletedate IS NULL";
                deleted = api.Database.ExecuteNonQuery(sql, sqlParams);
            }
            catch (Exception ex)
            {
            }

            return deleted;
        }

        public bool Exists(long userid, Guid moduleguid, int chapterpartid)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@userid", Value = userid } );
                sqlParams.Add(new SqlParameter { DbType = DbType.Guid, ParameterName = "@moduleguid", Value = moduleguid });
                sqlParams.Add(new SqlParameter { DbType = DbType.Int32, ParameterName = "@chapterpartid", Value = chapterpartid });
                Paged<UserDataInternal> paged = List(0, 1, new string[] { "userid" ,"moduleguid", "chapterpartid" }, "userid=@userid AND moduleguid=@moduleguid AND chapterpartid=@chapterpartid", sqlParams, "id DESC");

                if (paged != null)
                {
                    UserDataInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public bool Exists(string Value, string Field, params long[] ExludeIds)
        {
            bool exists = false;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + Field, Value = Value });

                string where = string.Empty;
                if (ExludeIds != null && ExludeIds.Any())
                    where += $" AND id NOT IN ({string.Join(",", ExludeIds)})";

                Paged<UserDataInternal> paged = List(0, 1, new string[] { "id" }, Field + "=@" + Field + where, sqlParams, "id DESC");

                if (paged != null)
                {
                    UserDataInternal obj = paged.List.FirstOrDefault();
                    exists = (obj != null);
                }
            }
            catch (Exception ex)
            {
            }

            return exists;
        }

        public UserDataInternal Get(long Id, bool Deleted = false, params string[] Columns)
        {
            UserDataInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.Int64, ParameterName = "@id", Value = Id });

                Paged<UserDataInternal> paged = List(0, 1, Columns, "id=@id", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public UserDataInternal Get(string Email, bool Deleted = false, params string[] Columns)
        {
            UserDataInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = new List<SqlParameter>();
                sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@email", Value = Email });

                Paged<UserDataInternal> paged = List(0, 1, Columns, "email=@email", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public UserDataInternal Get(UserAuth UserAuth, bool Deleted = false, params string[] Columns)
        {
            UserDataInternal obj = null;

            try
            {
                List<SqlParameter> sqlParams = UserAuth.ToSqlParameters();

                Paged<UserDataInternal> paged = List(0, 1, Columns, "email=@email AND password=@password COLLATE Latin1_General_CS_AS", sqlParams, "id DESC", Deleted);
                if (paged != null)
                    obj = paged.List.FirstOrDefault();
            }
            catch (Exception ex)
            {
                obj = null;
            }

            return obj;
        }

        public Paged<UserDataInternal> List(int PageIndex, int PageLimit, string[] Columns = null, string Where = null, List<SqlParameter> SqlParams = null, string OrderBy = null, bool Deleted = false)
        {
            string deleted = (Deleted ? "NOT NULL" : "NULL");
            Where += (!string.IsNullOrWhiteSpace(Where) ? " AND " : "") + $"deletedate IS {deleted}";

            return api.GetList<UserDataInternal>(api.TableNames.UserData, PageIndex, PageLimit, Columns, Where, SqlParams, OrderBy, (paged) =>
            {
                if (paged.List.Count > 0)
                {
                }
            });
        }
        public bool Update(UserDataInternal UserData)
        {
            bool updated = false;

            try
            {
                if (UserData.ModuleGuid != Guid.Empty)
                {
                    List<SqlParameter> sqlParams = UserData.ToSqlParameters();

                    // Update object
                    string sql = $"UPDATE  {api.TableNames.UserData}  SET updatedate=GETUTCDATE(), data=@data, done=@done WHERE userid=@userid AND moduleguid=@moduleguid AND chapterpartid=@chapterpartid AND deletedate IS NULL";
                    updated = api.Database.ExecuteNonQuery(sql, sqlParams);
                }
            }
            catch (Exception ex)
            {
            }

            return updated;
        }
        #endregion
    }
}