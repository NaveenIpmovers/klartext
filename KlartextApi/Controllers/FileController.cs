﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using OtrohetsakutenApi.Helpers;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.Controllers
{

    [ApiController]
    [ApiVersion("1")]
    [Produces("application/json")]
    [Route("v{version:apiVersion}/[controller]")]
    public class FileController : ControllerBase
    {
        private ApiHelper api = null;
        private IConfig config = null;

        public FileController(IConfig Config, IHttpContextAccessor Context)
        {
            api = new ApiHelper(Config);
            config = Config;

            api.SetBaseUrl(Context);
        }

        /// <summary>
        /// Write Files
        /// </summary>
        /// <param name="jsonData"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(200)]
        [ProducesResponseType(302)]
        [ProducesResponseType(400, Type = typeof(List<BadRequestError>))]
        [Route("")]
        public IActionResult Add(FileSave jsonData)
        {
            try
            {
                // Check request
                List<BadRequestError> badRequests = new List<BadRequestError>();
                if (jsonData == null)
                    badRequests.Add(new BadRequestError { Error = "JSON", Location = BadRequestError.ErrorLocation.Body, Message = BadRequestError.ErrorMessage.Required });
                else
                {
                    if (string.IsNullOrWhiteSpace(jsonData.DriverName))
                        badRequests.Add(new BadRequestError { Error = "Driver Name is Required", Location = BadRequestError.ErrorLocation.Body, Message = BadRequestError.ErrorMessage.Required });
                    if (string.IsNullOrWhiteSpace(jsonData.FileName))
                        badRequests.Add(new BadRequestError { Error = "FileName is Required", Location = BadRequestError.ErrorLocation.Body, Message = BadRequestError.ErrorMessage.Required });
                }
                if (badRequests.Count > 0)
                    return BadRequest(badRequests);
                else
                {
                    //Logging testing
                    //string jsonData = @"{  
                    //                    'DriverName':'Naveen Joshi',  
                    //                    'Text':'Cordinates 2.1, 2.2',
                    //                    'FileName':'Sample File'
                    //                    }";
                    //var userObj = JObject.Parse(jsonData);
                    var driverName = Convert.ToString(jsonData.DriverName);
                    var fileName = Convert.ToString(jsonData.FileName) + ".txt";
                    var msg = Convert.ToString(jsonData.Message);
                    var date = DateTime.Now.ToString("dd-MM-yyyy");
                    //End
                    api.Log(driverName, fileName, msg, date);

                }
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Ok();
        }

        /// <summary>
        /// Delete Logs
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        [ProducesResponseType(200)]
        [ProducesResponseType(302)]
        [ProducesResponseType(400, Type = typeof(List<BadRequestError>))]
        [Route("")]
        public IActionResult Delete()
        {
            try
            {
                api.DeleteLog();
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Ok();
        }
    }

}