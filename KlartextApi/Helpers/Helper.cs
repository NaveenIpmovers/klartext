﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for Helper
/// </summary>
public class Helper
{
    #region Variables
    private HelperDatabase database = null;
    private HelperHttp http = null;
    #endregion

    #region Properties
    public HelperDatabase Database
    {
        get
        {
            if (database == null)
                database = new HelperDatabase();
            return database;
        }
    }

    public HelperHttp HTTP
    {
        get
        {
            if (http == null)
                http = new HelperHttp();
            return http;
        }
    }
    #endregion

    #region Public methods
    public string GeneratePassword(int Length, IEnumerable<string> CharCollectionKeys, bool ExcludeSimilar = false)
    {
        string password = string.Empty;

        try
        {
            if (Length > 0)
            {
                // Create lists
                Dictionary<string, string> charCollections = new Dictionary<string, string>
                {
                    { "AlphaLowerCase", "abcdefghijklmnopqrstuvwxyz" },
                    { "AlphaUpperCase", "ABCDEFGHIJKLMNOPQRSTUVWXYZ" },
                    { "Numeric", "0123456789" }
                };

                if (ExcludeSimilar)
                    charCollections = new Dictionary<string, string>
                    {
                        { "AlphaLowerCase", "abcdefghjkmnpqrstuvwxyz" },
                        { "AlphaUpperCase", "ABCDEFGHJKMNPQRSTUVWXYZ" },
                        { "Numeric", "23456789" }
                    };

                // Add char collection to list
                ArrayList list = new ArrayList();
                if (CharCollectionKeys != null)
                    foreach (string key in CharCollectionKeys)
                        if (charCollections.ContainsKey(key))
                            list.AddRange(charCollections[key].ToCharArray());

                if (list.Count > 0)
                {
                    // Get random chars
                    Random rand = new Random();
                    while (password.Length < Length)
                        password += list[rand.Next(list.Count - 1)].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            password = string.Empty;
        }

        return password;
    }
    #endregion
}

public class HelperDatabase
{
    #region Variables
    private Dictionary<string, SqlConnection> connections = null;
    #endregion

    #region Properties
    public Dictionary<string, SqlConnection> Connections
    {
        get
        {
            if (connections == null)
                connections = new Dictionary<string, SqlConnection>();
            return connections;
        }
        set
        {
            connections = value;
        }
    }

    public string DefaultConnectionName { get; set; }
    #endregion

    #region Public methods
    public bool ExecuteNonQuery(string Sql)
    {
        return ExecuteNonQuery(Sql, null, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteNonQuery(string Sql, out int AffectedRows)
    {
        return ExecuteNonQuery(Sql, null, Connections[DefaultConnectionName], out AffectedRows);
    }

    public bool ExecuteNonQuery(string Sql, SqlConnection Connection)
    {
        return ExecuteNonQuery(Sql, null, Connection, out _);
    }

    public bool ExecuteNonQuery(string Sql, SqlConnection Connection, out int AffectedRows)
    {
        return ExecuteNonQuery(Sql, null, Connection, out AffectedRows);
    }

    public bool ExecuteNonQuery(string Sql, List<SqlParameter> Parameters)
    {
        return ExecuteNonQuery(Sql, Parameters, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteNonQuery(string Sql, List<SqlParameter> Parameters, out int AffectedRows)
    {
        return ExecuteNonQuery(Sql, Parameters, Connections[DefaultConnectionName], out AffectedRows);
    }

    public bool ExecuteNonQuery(string Sql, List<SqlParameter> Parameters, SqlConnection Connection)
    {
        return ExecuteNonQuery(Sql, Parameters, Connection, out _);
    }

    public bool ExecuteNonQuery(string Sql, List<SqlParameter> Parameters, SqlConnection Connection, out int AffectedRows)
    {
        bool success = false;
        AffectedRows = 0;

        try
        {
            // Open connection
            if (Connection.State != ConnectionState.Open)
                Connection.Open();

            // Execute SQL
            SqlCommand cmd = new SqlCommand(Sql, Connection);
            if (Parameters != null && Parameters.Any())
                foreach (SqlParameter param in Parameters)
                    cmd.Parameters.Add(new SqlParameter { DbType = param.DbType, ParameterName = param.ParameterName, Value = param.Value });
            AffectedRows = cmd.ExecuteNonQuery();

            // Set success
            success = true;
        }
        catch (Exception ex)
        {
        }
        finally
        {
            // Close connection
            if (Connection != null && Connection.State != ConnectionState.Closed)
                Connection.Close();
        }

        return success;
    }

    public bool ExecuteQuery(string Sql)
    {
        return ExecuteQuery(Sql, null, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteQuery(string Sql, out DataTable DataTable)
    {
        return ExecuteQuery(Sql, null, Connections[DefaultConnectionName], out DataTable);
    }

    public bool ExecuteQuery(string Sql, SqlConnection Connection)
    {
        return ExecuteQuery(Sql, null, Connection, out _);
    }

    public bool ExecuteQuery(string Sql, SqlConnection Connection, out DataTable DataTable)
    {
        return ExecuteQuery(Sql, null, Connection, out DataTable);
    }

    public bool ExecuteQuery(string Sql, List<SqlParameter> Parameters)
    {
        return ExecuteQuery(Sql, Parameters, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteQuery(string Sql, List<SqlParameter> Parameters, out DataTable DataTable)
    {
        return ExecuteQuery(Sql, Parameters, Connections[DefaultConnectionName], out DataTable);
    }

    public bool ExecuteQuery(string Sql, List<SqlParameter> Parameters, SqlConnection Connection)
    {
        return ExecuteQuery(Sql, null, Connection, out _);
    }

    public bool ExecuteQuery(string Sql, List<SqlParameter> Parameters, SqlConnection Connection, out DataTable DataTable)
    {
        bool success = false;
        DataTable = null;

        try
        {
            // Open connection
            if (Connection.State != ConnectionState.Open)
                Connection.Open();

            // Execute SQL
            SqlDataAdapter ad = new SqlDataAdapter(Sql, Connection);
            if (Parameters != null && Parameters.Any())
                foreach (SqlParameter param in Parameters)
                    ad.SelectCommand.Parameters.Add(new SqlParameter { DbType = param.DbType, ParameterName = param.ParameterName, Value = param.Value });
            DataTable = new DataTable();
            ad.Fill(DataTable);

            // Set success
            success = true;
        }
        catch (Exception ex)
        {
            DataTable = null;
        }
        finally
        {
            // Close connection
            if (Connection != null && Connection.State != ConnectionState.Closed)
                Connection.Close();
        }

        return success;
    }

    public bool ExecuteScalar(string Sql)
    {
        return ExecuteScalar(Sql, null, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteScalar(string Sql, out object Object)
    {
        return ExecuteScalar(Sql, null, Connections[DefaultConnectionName], out Object);
    }

    public bool ExecuteScalar(string Sql, SqlConnection Connection)
    {
        return ExecuteScalar(Sql, null, Connection, out _);
    }

    public bool ExecuteScalar(string Sql, SqlConnection Connection, out object Object)
    {
        return ExecuteScalar(Sql, null, Connection, out Object);
    }

    public bool ExecuteScalar(string Sql, List<SqlParameter> Parameters)
    {
        return ExecuteScalar(Sql, Parameters, Connections[DefaultConnectionName], out _);
    }

    public bool ExecuteScalar(string Sql, List<SqlParameter> Parameters, out object Object)
    {
        return ExecuteScalar(Sql, Parameters, Connections[DefaultConnectionName], out Object);
    }

    public bool ExecuteScalar(string Sql, List<SqlParameter> Parameters, SqlConnection Connection)
    {
        return ExecuteScalar(Sql, Parameters, Connection, out _);
    }

    public bool ExecuteScalar(string Sql, List<SqlParameter> Parameters, SqlConnection Connection, out object Object)
    {
        bool success = false;
        Object = null;

        try
        {
            // Open connection
            if (Connection.State != ConnectionState.Open)
                Connection.Open();

            // Execute SQL
            SqlCommand cmd = new SqlCommand(Sql, Connection);
            if (Parameters != null && Parameters.Any())
                foreach (SqlParameter param in Parameters)
                    cmd.Parameters.Add(new SqlParameter { DbType = param.DbType, ParameterName = param.ParameterName, Value = param.Value });
            Object = cmd.ExecuteScalar();

            // Set success
            success = true;
        }
        catch (Exception ex)
        {
            Object = null;
        }
        finally
        {
            // Close connection
            if (Connection != null && Connection.State != ConnectionState.Closed)
                Connection.Close();
        }

        return success;
    }
    #endregion
}

public static class HelperExtensions
{
    #region Variables
    private static string encryptionHashAlgorithm = string.Empty;
    private static int encryptionKeySize = 0;
    private static int encryptionPasswordIteration = 0;
    private static string encryptionSalt = string.Empty;
    private static string encryptionVector = string.Empty;
    #endregion

    #region Properties
    public static string EncryptionHashAlgorithm
    {
        get
        {
            if (string.IsNullOrWhiteSpace(encryptionHashAlgorithm))
            {
                /*string key = "EncryptionHashAlgorithm";
                if (!string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[key]))
                    encryptionHashAlgorithm = WebConfigurationManager.AppSettings[key];
                else if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                    encryptionHashAlgorithm = ConfigurationManager.AppSettings[key];*/
            }

            if (string.IsNullOrWhiteSpace(encryptionHashAlgorithm))
                encryptionHashAlgorithm = "SHA512";

            return encryptionHashAlgorithm;
        }
    }

    public static int EncryptionKeySize
    {
        get
        {
            if (encryptionKeySize == 0)
            {
                /*string key = "EncryptionKeySize";
                if (!string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[key]))
                    encryptionKeySize = Convert.ToInt32(WebConfigurationManager.AppSettings[key]);
                else if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                    encryptionKeySize = Convert.ToInt32(ConfigurationManager.AppSettings[key]);*/
            }

            if (encryptionKeySize == 0)
                encryptionKeySize = 256;

            return encryptionKeySize;
        }
    }

    public static int EncryptionPasswordIteration
    {
        get
        {
            if (encryptionPasswordIteration == 0)
            {
                /*string key = "EncryptionPasswordIteration";
                if (!string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[key]))
                    encryptionPasswordIteration = Convert.ToInt32(WebConfigurationManager.AppSettings[key]);
                else if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                    encryptionPasswordIteration = Convert.ToInt32(ConfigurationManager.AppSettings[key]);*/
            }

            if (encryptionPasswordIteration == 0)
                encryptionPasswordIteration = 256;

            return encryptionPasswordIteration;
        }
    }

    public static string EncryptionSalt
    {
        get
        {
            if (string.IsNullOrWhiteSpace(encryptionSalt))
            {
                /*string key = "EncryptionSalt";
                if (!string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[key]))
                    encryptionSalt = WebConfigurationManager.AppSettings[key];
                else if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                    encryptionSalt = ConfigurationManager.AppSettings[key];*/
            }

            if (string.IsNullOrWhiteSpace(encryptionSalt))
                encryptionSalt = "nUc@phuS";

            return encryptionSalt;
        }
    }

    public static string EncryptionVector
    {
        get
        {
            if (string.IsNullOrWhiteSpace(encryptionVector))
            {
                /*string key = "EncryptionVector";
                if (!string.IsNullOrWhiteSpace(WebConfigurationManager.AppSettings[key]))
                    encryptionVector = WebConfigurationManager.AppSettings[key];
                else if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings[key]))
                    encryptionVector = ConfigurationManager.AppSettings[key];*/
            }

            if (string.IsNullOrWhiteSpace(encryptionVector))
                encryptionVector = "baqa3H?BraGakePu";

            return encryptionVector;
        }
    }
    #endregion

    #region DataTable extension methods
    public static List<T> ToList<T>(this DataTable DataTable, bool? DatesIsUtc = true)
    {
        List<string> columnNames = DataTable.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToList();
        PropertyInfo[] props = typeof(T).GetProperties();

        List<T> list = DataTable.AsEnumerable().Select(row =>
        {
            T obj = Activator.CreateInstance<T>();

            foreach (PropertyInfo prop in props)
            {
                if (columnNames.Contains(prop.Name.ToLower()))
                {
                    if (Nullable.GetUnderlyingType(prop.PropertyType) != null)
                        prop.SetValue(obj, row[prop.Name.ToLower()] == DBNull.Value ? null : Convert.ChangeType(row[prop.Name.ToLower()], Nullable.GetUnderlyingType(prop.PropertyType)));
                    else if (prop.PropertyType.BaseType == typeof(Enum))
                        prop.SetValue(obj, Enum.Parse(prop.PropertyType, row[prop.Name.ToLower()].ToString(), true));
                    else if (prop.PropertyType.IsArray)
                    {
                        var objArray = (row[prop.Name.ToLower()] == DBNull.Value ? null : row[prop.Name.ToLower()].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ChangeType(x, prop.PropertyType.GetElementType())).ToArray());
                        var typeArray = (objArray == null ? null : Array.CreateInstance(prop.PropertyType.GetElementType(), objArray.Length));
                        if (typeArray != null)
                            Array.Copy(objArray, typeArray, objArray.Length);
                        prop.SetValue(obj, typeArray);
                    }
                    else if (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        var objList = (row[prop.Name.ToLower()] == DBNull.Value ? null : row[prop.Name.ToLower()].ToString().Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ChangeType(x, prop.PropertyType.GetGenericArguments()[0])).ToList());
                        var typeList = (objList == null ? null : Activator.CreateInstance(typeof(List<>).MakeGenericType(prop.PropertyType.GetGenericArguments()[0])));
                        if (typeList != null)
                            foreach (object item in objList)
                                ((IList)typeList).Add(item);
                        prop.SetValue(obj, typeList);
                    }
                    else
                        prop.SetValue(obj, row[prop.Name.ToLower()] == DBNull.Value ? null : Convert.ChangeType(row[prop.Name.ToLower()], obj.GetType().GetProperty(prop.Name).PropertyType));

                    if (DatesIsUtc.HasValue)
                    {
                        if (prop.PropertyType == typeof(DateTime?))
                        {
                            DateTime? date = (row[prop.Name.ToLower()] == DBNull.Value ? null : (DateTime?)Convert.ToDateTime(row[prop.Name.ToLower()]));

                            if (date.HasValue)
                            {
                                if (DatesIsUtc.HasValue)
                                    date = DateTime.SpecifyKind(date.Value, DateTimeKind.Utc);
                                else
                                    date = DateTime.SpecifyKind(date.Value, DateTimeKind.Local);

                                prop.SetValue(obj, date);
                            }
                        }
                        else if (prop.PropertyType == typeof(DateTime))
                        {
                            DateTime date = Convert.ToDateTime(row[prop.Name.ToLower()]);

                            if (DatesIsUtc.HasValue)
                                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
                            else
                                date = DateTime.SpecifyKind(date, DateTimeKind.Local);

                            prop.SetValue(obj, date);
                        }
                    }
                }
            }

            return obj;
        }).ToList();

        return list;
    }
    #endregion

    #region Object extension methods
    public static bool IsDate(this object Object)
    {
        bool success = false;

        try
        {
            DateTime date;
            List<Type> types = new List<Type> { typeof(DateTime) };
            success = (types.Contains(Object.GetType()) || (Object != null && DateTime.TryParse(Object.ToString(), out date)));
        }
        catch (Exception ex)
        {
        }

        return success;
    }

    public static bool IsNumber(this object Object, bool IncludeDividers = false)
    {
        bool success = false;

        try
        {
            List<Type> types = new List<Type> { typeof(Decimal), typeof(Double), typeof(Int16), typeof(Int32), typeof(Int64) };
            success = (types.Contains(Object.GetType()) || (Object != null && Regex.IsMatch(Object.ToString(), (IncludeDividers ? @"^-?\d+((.|,)\d+)?$" : @"^-?\d+$"))));
        }
        catch (Exception ex)
        {
        }

        return success;
    }

    public static bool IsNumeric(this object Object)
    {
        bool success = false;

        try
        {
            success = (Object != null && Regex.IsMatch(Object.ToString(), @"^\d+$"));
        }
        catch (Exception ex)
        {
        }

        return success;
    }

    public static T Merge<T>(this T Target, object Source, bool AllowNull = false)
    {
        return Target.Merge(Source, null, AllowNull: AllowNull);
    }

    public static T Merge<T>(this T Target, object Source, IEnumerable<string> Properties, bool ExcludeProperties = true, bool AllowNull = false)
    {
        if (Properties == null)
            Properties = new string[0];

        Dictionary<PropertyInfo, PropertyInfo> pis = new Dictionary<PropertyInfo, PropertyInfo>();
        PropertyInfo[] target = Target.GetType().GetProperties();
        PropertyInfo[] source = Source.GetType().GetProperties();

        foreach (PropertyInfo pi in target)
        {
            if ((ExcludeProperties && !Properties.Any(x => x.ToLower() == pi.Name.ToLower())) || (!ExcludeProperties && Properties.Any(x => x.ToLower() == pi.Name.ToLower())))
            {
                var temp = source.FirstOrDefault(x => x.Name.ToLower() == pi.Name.ToLower() && x.PropertyType == x.PropertyType);
                if (temp != null)
                    pis.Add(pi, temp);
            }
        }

        foreach (KeyValuePair<PropertyInfo, PropertyInfo> kvp in pis)
        {
            var value = kvp.Value.GetValue(Source);
            if (AllowNull || value != null)
                kvp.Key.SetValue(Target, value);
        }

        return Target;
    }

    public static void ToEmptyStrings(this object Object)
    {
        foreach (PropertyInfo pi in Object.GetType().GetProperties())
            if (pi.PropertyType == typeof(String) && pi.GetValue(Object) == null)
                pi.SetValue(Object, "");
    }

    public static List<string> ToSqlColumns(this object Object)
    {
        return Object.ToSqlColumns(null, null);
    }

    public static List<string> ToSqlColumns(this object Object, bool LowerCaseNames)
    {
        return Object.ToSqlColumns(null, null, LowerCaseNames);
    }

    public static List<string> ToSqlColumns(this object Object, string AppendTag)
    {
        return Object.ToSqlColumns(AppendTag, null);
    }

    public static List<string> ToSqlColumns(this object Object, IEnumerable<string> ExcludeProperties)
    {
        return Object.ToSqlColumns(null, ExcludeProperties);
    }

    public static List<string> ToSqlColumns(this object Object, string AppendTag, bool LowerCaseNames)
    {
        return Object.ToSqlColumns(AppendTag, null, LowerCaseNames);
    }

    public static List<string> ToSqlColumns(this object Object, IEnumerable<string> ExcludeProperties, bool LowerCaseNames)
    {
        return Object.ToSqlColumns(null, ExcludeProperties, LowerCaseNames);
    }

    public static List<string> ToSqlColumns(this object Object, string AppendTag, IEnumerable<string> ExcludeProperties, bool LowerCaseNames = true)
    {
        List<SqlParameter> sqlParams = Object.ToSqlParameters(AppendTag, ExcludeProperties, LowerCaseNames);
        List<string> sqlColumns = sqlParams.Select(x => x.ParameterName.Substring(1)).ToList();
        return sqlColumns;
    }

    public static List<SqlParameter> ToSqlParameters(this object Object)
    {
        return Object.ToSqlParameters(null, null);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, bool LowerCaseNames)
    {
        return Object.ToSqlParameters(null, null, LowerCaseNames);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, string AppendTag)
    {
        return Object.ToSqlParameters(AppendTag, null);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, IEnumerable<string> ExcludeProperties)
    {
        return Object.ToSqlParameters(null, ExcludeProperties);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, string AppendTag, bool LowerCaseNames)
    {
        return Object.ToSqlParameters(AppendTag, null, LowerCaseNames);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, IEnumerable<string> ExcludeProperties, bool LowerCaseNames)
    {
        return Object.ToSqlParameters(null, ExcludeProperties, LowerCaseNames);
    }

    public static List<SqlParameter> ToSqlParameters(this object Object, string AppendTag, IEnumerable<string> ExcludeProperties, bool LowerCaseNames = true)
    {
        List<SqlParameter> sqlParams = new List<SqlParameter>();

        Dictionary<Type, DbType> typeMap = new Dictionary<Type, DbType>();
        typeMap[typeof(byte)] = DbType.Byte;
        typeMap[typeof(sbyte)] = DbType.SByte;
        typeMap[typeof(short)] = DbType.Int16;
        typeMap[typeof(ushort)] = DbType.Int16;
        typeMap[typeof(int)] = DbType.Int32;
        typeMap[typeof(uint)] = DbType.Int32;
        typeMap[typeof(long)] = DbType.Int64;
        typeMap[typeof(ulong)] = DbType.Int64;
        typeMap[typeof(float)] = DbType.Single;
        typeMap[typeof(double)] = DbType.Double;
        typeMap[typeof(decimal)] = DbType.Decimal;
        typeMap[typeof(bool)] = DbType.Boolean;
        typeMap[typeof(string)] = DbType.String;
        typeMap[typeof(char)] = DbType.StringFixedLength;
        typeMap[typeof(Guid)] = DbType.Guid;
        typeMap[typeof(DateTime)] = DbType.DateTime;
        typeMap[typeof(DateTimeOffset)] = DbType.DateTimeOffset;
        typeMap[typeof(byte[])] = DbType.Binary;
        typeMap[typeof(byte?)] = DbType.Byte;
        typeMap[typeof(sbyte?)] = DbType.SByte;
        typeMap[typeof(short?)] = DbType.Int16;
        typeMap[typeof(ushort?)] = DbType.UInt16;
        typeMap[typeof(int?)] = DbType.Int32;
        typeMap[typeof(uint?)] = DbType.Int32;
        typeMap[typeof(long?)] = DbType.Int64;
        typeMap[typeof(ulong?)] = DbType.Int64;
        typeMap[typeof(float?)] = DbType.Single;
        typeMap[typeof(double?)] = DbType.Double;
        typeMap[typeof(decimal?)] = DbType.Decimal;
        typeMap[typeof(bool?)] = DbType.Boolean;
        typeMap[typeof(char?)] = DbType.StringFixedLength;
        typeMap[typeof(Guid?)] = DbType.Guid;
        typeMap[typeof(DateTime?)] = DbType.DateTime;
        typeMap[typeof(DateTimeOffset?)] = DbType.DateTimeOffset;

        if (ExcludeProperties == null)
            ExcludeProperties = new string[0];

        foreach (PropertyInfo pi in Object.GetType().GetProperties())
        {
            if (ExcludeProperties.Any(x => x.ToLower() == pi.Name.ToLower()))
                continue;
            else if (ExcludeProperties.Where(x => x.StartsWith("*")).Select(x => x.Replace("*", "")).Any(x => pi.Name.ToLower().EndsWith(x.ToLower())))
                continue;
            else if (ExcludeProperties.Where(x => x.EndsWith("*")).Select(x => x.Replace("*", "")).Any(x => pi.Name.ToLower().StartsWith(x.ToLower())))
                continue;

            if ((typeMap.ContainsKey(pi.PropertyType) || pi.PropertyType.IsArray || (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(List<>)) || pi.PropertyType.BaseType == typeof(Enum)))
            {
                if (pi.PropertyType.IsArray)
                    sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = string.Join(",", ((Array)pi.GetValue(Object) ?? new object[0]).Cast<object>()) });
                else if (pi.PropertyType.IsGenericType && pi.PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    sqlParams.Add(new SqlParameter { DbType = DbType.String, ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = string.Join(",", ((IList)pi.GetValue(Object) ?? new object[0]).Cast<object>()) });
                else if (pi.PropertyType == typeof(DateTime) && ((DateTime)pi.GetValue(Object) < SqlDateTime.MinValue.Value || (DateTime)pi.GetValue(Object) > SqlDateTime.MaxValue.Value))
                    sqlParams.Add(new SqlParameter { DbType = typeMap[typeof(DateTime)], ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = DBNull.Value });
                else if (pi.PropertyType == typeof(DateTime?) && ((DateTime?)pi.GetValue(Object)).HasValue && (((DateTime?)pi.GetValue(Object)).Value < SqlDateTime.MinValue.Value || ((DateTime?)pi.GetValue(Object)).Value > SqlDateTime.MaxValue.Value))
                    sqlParams.Add(new SqlParameter { DbType = typeMap[typeof(DateTime)], ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = DBNull.Value });
                else if (pi.PropertyType.BaseType == typeof(Enum))
                    sqlParams.Add(new SqlParameter { DbType = typeMap[typeof(Int32)], ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = (int)pi.GetValue(Object) });
                else
                    sqlParams.Add(new SqlParameter { DbType = typeMap[pi.PropertyType], ParameterName = "@" + pi.Name.ToLower() + AppendTag, Value = (pi.GetValue(Object) ?? DBNull.Value) });
            }
        }

        return sqlParams;
    }
    #endregion

    #region String extension methods
    public static string Decrypt(this string String, string Password)
    {
        string s = string.Empty;

        try
        {
            s = decrypt(String, Password, EncryptionSalt, EncryptionHashAlgorithm, EncryptionPasswordIteration, EncryptionVector, EncryptionKeySize);
        }
        catch (Exception ex)
        {
        }

        return s;
    }

    public static string Encrypt(this string String, string Password)
    {
        string s = string.Empty;

        try
        {
            s = encrypt(String, Password, EncryptionSalt, EncryptionHashAlgorithm, EncryptionPasswordIteration, EncryptionVector, EncryptionKeySize);
        }
        catch (Exception ex)
        {
        }

        return s;
    }

    public static string ToSHA256HashString(this string String)
    {
        return String.ToSHA256HashString("X2");
    }

    public static string ToSHA256HashString(this string String, string Format)
    {
        HashAlgorithm algorithm = SHA256.Create();
        StringBuilder sb = new StringBuilder();
        foreach (byte b in algorithm.ComputeHash(Encoding.UTF8.GetBytes(String)))
            sb.Append(b.ToString(Format));
        return sb.ToString();
    }

    public static string ToSHA512HashString(this string String)
    {
        return String.ToSHA512HashString("X2");
    }

    public static string ToSHA512HashString(this string String, string Format)
    {
        HashAlgorithm algorithm = SHA512.Create();
        StringBuilder sb = new StringBuilder();
        foreach (byte b in algorithm.ComputeHash(Encoding.UTF8.GetBytes(String)))
            sb.Append(b.ToString(Format));
        return sb.ToString();
    }

    public static string ToSQLSafe(this string String)
    {
        return String.Replace("'", "''");
    }
    #endregion

    #region Private methods
    private static string decrypt(string cipherText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
    {
        // Convert strings defining encryption key characteristics into byte
        // arrays. Let us assume that strings only contain ASCII codes.
        // If strings include Unicode characters, use Unicode, UTF7, or UTF8
        // encoding.
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        // Convert our ciphertext into a byte array.
        byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

        // First, we must create a password, from which the key will be 
        // derived. This password will be generated from the specified 
        // passphrase and salt value. The password will be created using
        // the specified hash algorithm. Password creation can be done in
        // several iterations.
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        // Use the password to generate pseudo-random bytes for the encryption
        // key. Specify the size of the key in bytes (instead of bits).
        byte[] keyBytes = password.GetBytes(keySize / 8);

        // Create uninitialized Rijndael encryption object.
        RijndaelManaged symmetricKey = new RijndaelManaged();

        // It is reasonable to set encryption mode to Cipher Block Chaining
        // (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC;

        // Generate decryptor from the existing key bytes and initialization 
        // vector. Key size will be defined based on the number of the key 
        // bytes.
        ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes);

        // Define memory stream which will be used to hold encrypted data.
        MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

        // Define cryptographic stream (always use Read mode for encryption).
        CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);

        // Since at this point we don't know what the size of decrypted data
        // will be, allocate the buffer long enough to hold ciphertext;
        // plaintext is never longer than ciphertext.
        byte[] plainTextBytes = new byte[cipherTextBytes.Length];

        // Start decrypting.
        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

        // Close both streams.
        memoryStream.Close();
        cryptoStream.Close();

        // Convert decrypted data into a string. 
        // Let us assume that the original plaintext string was UTF8-encoded.
        string plainText = Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);

        // Return decrypted string.   
        return plainText;
    }

    private static string encrypt(string plainText, string passPhrase, string saltValue, string hashAlgorithm, int passwordIterations, string initVector, int keySize)
    {
        // Convert strings into byte arrays.
        // Let us assume that strings only contain ASCII codes.
        // If strings include Unicode characters, use Unicode, UTF7, or UTF8 
        // encoding.
        byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
        byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

        // Convert our plaintext into a byte array.
        // Let us assume that plaintext contains UTF8-encoded characters.
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        // First, we must create a password, from which the key will be derived.
        // This password will be generated from the specified passphrase and 
        // salt value. The password will be created using the specified hash 
        // algorithm. Password creation can be done in several iterations.
        PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);

        // Use the password to generate pseudo-random bytes for the encryption
        // key. Specify the size of the key in bytes (instead of bits).
        byte[] keyBytes = password.GetBytes(keySize / 8);

        // Create uninitialized Rijndael encryption object.
        RijndaelManaged symmetricKey = new RijndaelManaged();

        // It is reasonable to set encryption mode to Cipher Block Chaining
        // (CBC). Use default options for other symmetric key parameters.
        symmetricKey.Mode = CipherMode.CBC;

        // Generate encryptor from the existing key bytes and initialization 
        // vector. Key size will be defined based on the number of the key 
        // bytes.
        ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);

        // Define memory stream which will be used to hold encrypted data.
        MemoryStream memoryStream = new MemoryStream();

        // Define cryptographic stream (always use Write mode for encryption).
        CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
        // Start encrypting.
        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

        // Finish encrypting.
        cryptoStream.FlushFinalBlock();

        // Convert our encrypted data from a memory stream into a byte array.
        byte[] cipherTextBytes = memoryStream.ToArray();

        // Close both streams.
        memoryStream.Close();
        cryptoStream.Close();

        // Convert encrypted data into a base64-encoded string.
        string cipherText = Convert.ToBase64String(cipherTextBytes);

        // Return encrypted string.
        return cipherText;
    }
    #endregion
}

public class HelperHttp
{
    #region Public methods
    public HelperHttpResponse Get(HelperHttpRequest Request)
    {
        HelperHttpResponse response = new HelperHttpResponse();

        try
        {
            // Create web request
            HttpWebRequest httpRequest = (HttpWebRequest)HttpWebRequest.Create(Request.Uri);
            if (!string.IsNullOrWhiteSpace(Request.Accept))
                httpRequest.Accept = Request.Accept;
            httpRequest.AutomaticDecompression = Request.AutomaticDecompression;
            if (Request.ClientCertificates != null && Request.ClientCertificates.Count > 0)
                httpRequest.ClientCertificates.AddRange(Request.ClientCertificates);
            httpRequest.ContentLength = 0;
            if (!string.IsNullOrWhiteSpace(Request.ContentType))
                httpRequest.ContentType = Request.ContentType;
            if (Request.Cookies != null)
                httpRequest.CookieContainer = Request.Cookies;
            if (Request.Headers != null && Request.Headers.Count > 0)
                httpRequest.Headers.Add(Request.Headers);
            httpRequest.Method = Request.Method.ToString();
            if (!string.IsNullOrWhiteSpace(Request.UserAgent))
                httpRequest.UserAgent = Request.UserAgent;

            // Set content
            string content = string.Empty;
            if (Request.FormData != null && Request.FormData.Count > 0)
                content = string.Join("&", Request.FormData.AllKeys.Select(x => string.Format("{0}={1}", x, Request.FormData[x])));
            else if (!string.IsNullOrWhiteSpace(Request.Body))
                content = Request.Body;

            if (!string.IsNullOrWhiteSpace(content))
            {
                // Set data
                byte[] data = (Request.ContentEncoding != null ? Request.ContentEncoding : Encoding.ASCII).GetBytes(content);

                // Modify request
                httpRequest.ContentLength = data.Length;

                // Send data
                using (var stream = httpRequest.GetRequestStream())
                    stream.Write(data, 0, data.Length);
            }

            // Get response
            HttpWebResponse httpResponse = (HttpWebResponse)httpRequest.GetResponse();

            // Set response
            response.Response = httpResponse;
        }
        catch (WebException ex)
        {
            // Get response
            HttpWebResponse httpResponse = (HttpWebResponse)ex.Response;

            // Set exception
            response.Response = httpResponse;
            response.Exception = ex;
        }
        catch (Exception ex)
        {
            // Set exception
            response.Exception = ex;
        }

        return response;
    }

    public HelperHttpResponse GetString(HelperHttpRequest Request)
    {
        HelperHttpResponse response = new HelperHttpResponse();

        try
        {
            // Get response
            HelperHttpResponse httpResponse = Get(Request);

            if (httpResponse.Success)
            {
                // Set data and success
                response.Data = new StreamReader(httpResponse.Response.GetResponseStream()).ReadToEnd();
                response.Response = httpResponse.Response;
            }
            else
            {
                // Set exception
                if (httpResponse.Response != null)
                    response.Data = new StreamReader(httpResponse.Response.GetResponseStream()).ReadToEnd();
                response.Exception = httpResponse.Exception;
                response.Response = httpResponse.Response;
            }
        }
        catch (Exception ex)
        {
            // Set exception
            response.Exception = ex;
        }

        return response;
    }
    #endregion
}

public class HelperHttpRequest
{
    #region Enums
    public enum Methods
    {
        GET,
        DELETE,
        PATCH,
        POST,
        PUT
    }
    #endregion

    #region Properties
    public string Accept { get; set; }
    public DecompressionMethods AutomaticDecompression { get; set; }
    public string Body { get; set; }
    public X509CertificateCollection ClientCertificates { get; set; }
    public Encoding ContentEncoding { get; set; }
    public string ContentType { get; set; }
    public CookieContainer Cookies { get; set; }
    public NameValueCollection FormData { get; set; }
    public NameValueCollection Headers { get; set; }
    public Methods Method { get; set; }
    public Uri Uri { get; set; }
    public string UserAgent { get; set; }
    #endregion
}

public class HelperHttpResponse
{
    #region Properties
    public string Data { get; set; }
    public Exception Exception { get; set; }
    public HttpWebResponse Response { get; set; }
    public bool Success
    {
        get
        {
            return (Response != null && (int)Response.StatusCode >= 200 && (int)Response.StatusCode <= 299);
        }
    }
    #endregion
}