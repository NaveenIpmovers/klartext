﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using OtrohetsakutenApi.Models;

namespace OtrohetsakutenApi.Helpers
{
    public interface ITokenService
    {
        Token GenerateAccessToken(long UserId, User.UserType UserType, int ExpirationInSeconds);
    }

    public class TokenService : ITokenService
    {
        #region Variables
        private readonly AppSettings settings;
        #endregion

        #region Constructor
        public TokenService(IOptions<AppSettings> Settings)
        {
            settings = Settings.Value;
        }
        #endregion

        #region Public methods
        public Token GenerateAccessToken(long UserId, User.UserType UserType, int ExpirationInSeconds)
        {
            Token token = null;

            try
            {
                DateTime now = DateTime.UtcNow;
                DateTime expires = now.AddSeconds(ExpirationInSeconds);

                SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.Jwt.Secret));
                JwtSecurityToken securityToken = new JwtSecurityToken(null, null, new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, UserId.ToString()),
                    new Claim(ClaimTypes.Role, UserType.ToString())
                }, now, expires, new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature));
                string tokenString = new JwtSecurityTokenHandler().WriteToken(securityToken);

                token = new Token
                {
                    AccessToken = tokenString,
                    Date = now,
                    ExpiresIn = ExpirationInSeconds,
                    RefreshToken = generateRefreshToken(),
                    UserId = UserId
                };
            }
            catch (Exception ex)
            {
            }

            return token;
        }
        #endregion

        #region Private methods
        private string generateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (RandomNumberGenerator rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
        #endregion
    }
}