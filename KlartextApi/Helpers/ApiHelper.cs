﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Xml;
using OtrohetsakutenApi.Models;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Helpers
{
    public class ApiHelper : Helper
    {
        #region Enums
        public enum FilterBoolean : int
        {
            Any = 2,
            False = 0,
            True = 1
        }
        #endregion

        #region Variables
        private BL.BL bl = null;
        private IConfig config = null;
        private TableNames tableNames = null;
        private Dictionary<string, string> translationsEnUs = null;
        private Dictionary<string, string> translationsSvSe = null;
        #endregion

        #region Properties
    
        public string BaseUrl
        {
            get
            {
                return AppDomain.CurrentDomain.GetData("BaseUrl").ToString();
            }
        }

        public BL.BL BL
        {
            get
            {
                if (bl == null)
                    bl = new BL.BL(config);
                return bl;
            }
        }

        public string ContentRootPath
        {
            get
            {
                return AppDomain.CurrentDomain.GetData("ContentRootPath").ToString();
            }
        }

        public string DefaultLanguage { get { return config.Settings.Language.Default; } }

        public string[] Languages { get { return config.Settings.Language.Available; } }

        public string RegExpPassword { get { return @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$"; } }

        public List<User.UserType> Roles { get; set; }

        public TableNames TableNames
        {
            get
            {
                if (tableNames == null)
                    tableNames = new TableNames();
                return tableNames;
            }
        }

        public Dictionary<string, string> TranslationsEnUs
        {
            get
            {
                if (translationsEnUs == null)
                {
                    // Load XML
                    translationsEnUs = new Dictionary<string, string>();
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Path.Combine(ContentRootPath, "App_Data", "translations_en-US.xml"));
                    foreach (XmlNode translation in doc.SelectNodes("//translations/translation"))
                        translationsEnUs.Add(translation.SelectSingleNode("key").InnerText, translation.SelectSingleNode("value").InnerText);
                }

                return translationsEnUs;
            }
        }

        public Dictionary<string, string> TranslationsSvSe
        {
            get
            {
                if (translationsSvSe == null)
                {
                    // Load XML
                    translationsSvSe = new Dictionary<string, string>();
                    XmlDocument doc = new XmlDocument();
                    doc.Load(Path.Combine(ContentRootPath, "App_Data", "translations_sv-SE.xml"));
                    foreach (XmlNode translation in doc.SelectNodes("//translations/translation"))
                        translationsSvSe.Add(translation.SelectSingleNode("key").InnerText, translation.SelectSingleNode("value").InnerText);
                }

                return translationsSvSe;
            }
        }

        public string UploadPath
        {
            get
            {
                string path = Path.Combine(ContentRootPath, "Uploads");
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                return path;
            }
        }

        public string WebRootPath
        {
            get
            {
                return AppDomain.CurrentDomain.GetData("WebRootPath").ToString();
            }
        }
        #endregion

        #region Constructor
        public ApiHelper(IConfig Config)
        {
            config = Config;

            if (Config != null)
                Database.Connections.Add("ConnectionString", new SqlConnection(Config.Settings.Sql.ConnectionString));
            Database.DefaultConnectionName = "ConnectionString";
        }
        #endregion

        #region Public methods
        public string GetAccessToken(HttpContext Context)
        {
            string accessToken = null;

            try
            {
                string key = "Authorization";

                if (Context.Request.Headers.ContainsKey(key) && Context.Request.Headers[key].Any())
                {
                    // Get access token
                    string keyData = Context.Request.Headers[key].FirstOrDefault();
                    Match match = Regex.Match(keyData, @"^(\s+)?(bearer)((\s+)?:(\s+)?|\s+)(.+)(\s+)?$", RegexOptions.IgnoreCase);
                    if (match.Success)
                        accessToken = match.Groups[6].Value;
                }
            }
            catch (Exception ex)
            {
            }

            return accessToken;
        }

        public string GetCurrentLanguage(HttpContext Context)
        {
            string lang = DefaultLanguage;
            string temp = GetHeaderLanguage(Context);

            if (!string.IsNullOrWhiteSpace(temp))
                lang = temp;
            else
            {
                UserInternal user = GetCurrentUser(Context);
                if (user != null)
                    lang = user.Language;
            }

            return lang;
        }

        public UserInternal GetCurrentUser(HttpContext Context)
        {
            UserInternal user = null;
            long id = GetCurrentUserId(Context);

            if (id > 0)
                user = BL.User.Get(id);

            return user;
        }

        public long GetCurrentUserId(HttpContext Context)
        {
            long id = 0;

            try
            {
                // Get user id
                if (Context.User.Identity.IsAuthenticated)
                    id = Convert.ToInt64(Context.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value);
            }
            catch (Exception ex)
            {
            }

            return id;
        }

        public string GetHeaderLanguage(HttpContext Context)
        {
            string lang = null;

            try
            {
                // Get language
                string key = "Accept-Language";
                string header;

                if (Context.Request.Headers.ContainsKey(key) && Context.Request.Headers[key].Any() && !string.IsNullOrWhiteSpace(header = Context.Request.Headers[key].FirstOrDefault()))
                {
                    List<string> qualities = header.Split(',').Select(StringWithQualityHeaderValue.Parse).OrderByDescending(s => s.Quality.GetValueOrDefault(1)).Select(x => x.Value).ToList();
                    string temp = qualities.FirstOrDefault(x => Languages.Any(y => y.ToLower() == x.ToLower()));

                    if (!string.IsNullOrWhiteSpace(temp))
                        lang = Languages.FirstOrDefault(x => x.ToLower() == temp.ToLower());
                }
            }
            catch (Exception ex)
            {
            }

            return lang;
        }

        public Paged<T> GetList<T>(string SqlStatement, int PageIndex, int PageLimit, string PrimaryColumn, string[] Columns, List<SqlParameter> SqlParams, string OrderBy, Action<Paged<T>> PostAction = null)
        {
            Paged<T> paged = null;

            try
            {
                // Fix page index and page limit
                if (PageIndex < 0)
                    PageIndex = 0;
                if (PageLimit < 0)
                    PageLimit = 0;

                // Get filters
                string sqlColumns = "*";
                string sqlOrderBy = PrimaryColumn + " ASC";

                if (Columns != null && Columns.Any())
                    sqlColumns = string.Join(", ", Columns);

                if (!string.IsNullOrWhiteSpace(OrderBy))
                    sqlOrderBy = OrderBy;

                // Get count
                string sql = string.Format(SqlStatement, string.Format("COUNT({0})", PrimaryColumn));
                object objItems;

                if (Database.ExecuteScalar(sql, SqlParams, out objItems))
                {
                    paged = new Paged<T>();
                    paged.List = new List<T>();

                    if ((paged.TotalItems = Convert.ToInt32(objItems)) > 0)
                    {
                        // Get list
                        sql = string.Format(SqlStatement, sqlColumns);
                        sql += $" ORDER BY {sqlOrderBy}";
                        if (PageLimit > 0)
                        {
                            sql = string.Format(SqlStatement, $"TOP {PageLimit} {sqlColumns}");
                            sql += $" AND {PrimaryColumn} NOT IN (";
                            sql += string.Format(SqlStatement, $"TOP {(PageIndex * PageLimit)} {PrimaryColumn}");
                            sql += $" ORDER BY {sqlOrderBy}) ORDER BY {sqlOrderBy}";
                        }
                        DataTable dt = new DataTable();

                        if (Database.ExecuteQuery(sql, SqlParams, out dt))
                        {
                            paged.List = dt.ToList<T>();
                            paged.Items = paged.List.Count;
                            paged.PageIndex = PageIndex;
                            paged.TotalPages = (PageLimit > 0 ? Convert.ToInt32(Math.Ceiling((double)paged.TotalItems / PageLimit)) : 1);

                            // Execute post action
                            if (PostAction != null)
                                PostAction.Invoke(paged);
                        }
                        else
                            paged = null;
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return paged;
        }

        public Paged<T> GetList<T>(string TableName, int PageIndex, int PageLimit, string[] Columns, string Where, List<SqlParameter> SqlParams, string OrderBy, Action<Paged<T>> PostAction = null)
        {
            Paged<T> paged = null;

            // Get filters
            string sqlWhere = string.Empty;
            if (!string.IsNullOrWhiteSpace(Where))
                sqlWhere = " AND " + Where;

            string sql = $"SELECT {{0}} FROM {TableName} WHERE 1=1{sqlWhere}";

            paged = GetList<T>(sql, PageIndex, PageLimit, "id", Columns, SqlParams, OrderBy, PostAction);

            return paged;
        }

        public string GetOrderBy<T>(string Sort, string DefaultOrderBy)
        {
            string orderBy = string.Empty;

            try
            {
                if (!string.IsNullOrWhiteSpace(Sort))
                {
                    // Validate sort
                    Dictionary<string, string> sorts = Regex.Matches(Sort, @"\w+(\s+)?:(\s+)?(asc|desc)(ending)?", RegexOptions.IgnoreCase).Select(x => x.Groups[0].Value.Split(':')).ToDictionary(x => x[0].Trim(), x => x[1].Trim());

                    if (sorts.Count > 0)
                    {
                        // Get property data member names
                        List<string> names = typeof(T).GetProperties().SelectMany(x => x.GetCustomAttributes(typeof(DataMemberAttribute), true).Cast<DataMemberAttribute>().Select(y => y.Name)).Distinct().ToList();

                        // Filter sort
                        sorts = sorts.Where(x => names.Any(y => y.ToLower() == x.Key.ToLower())).ToDictionary(x => x.Key, x => x.Value);

                        // Set order by
                        if (sorts.Count > 0)
                            orderBy = string.Join(", ", sorts.Select(x => x.Key.ToLower() + " " + x.Value.ToLower().Replace("ending", "").ToUpper()));
                    }
                }
            }
            catch (Exception ex)
            {
            }

            // Set order by
            if (string.IsNullOrWhiteSpace(orderBy))
                orderBy = DefaultOrderBy;

            return orderBy;
        }

        public void Log(string DriverName, string FileName, string Message, string LineTimeStamp = "yyyy-MM-dd HH:mm:ss.fff")
        {
            try
            {
                string logPath = Path.Combine(ContentRootPath, "Logs/" + DriverName + "/" + LineTimeStamp);
                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                using (StreamWriter sw = new StreamWriter(Path.Combine(logPath, FileName), true))
                {
                    if (!string.IsNullOrWhiteSpace(LineTimeStamp))
                        sw.WriteLine($"{DateTime.Now.ToString(LineTimeStamp)} - {Message}");
                    else
                        sw.WriteLine(Message);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void DeleteLog()
        {
            try
            {
                string logPath = Path.Combine(ContentRootPath, "Logs/");
                if (Directory.Exists(logPath))
                    Directory.Delete(logPath, true);
               
            }
            catch (Exception ex)
            {
            }
        }
        public bool SendMail(string Subject, string HtmlBody, params string[] To)
        {
            return SendMail(Subject, HtmlBody, true, To);
        }

        public bool SendMail(string Subject, string Body, bool IsBodyHtml, params string[] To)
        {
            bool sent = false;

            try
            {
                // Configure message
                MailMessage msg = new MailMessage();
                msg.Body = Body;

                msg.From = new MailAddress(config.Settings.Email.Sender, config.Settings.Email.SenderName);
                msg.IsBodyHtml = IsBodyHtml;
                msg.Subject = Subject;
                foreach (string email in To)
                    msg.To.Add(email);

                // Configure SMTP
                SmtpClient smtp = new SmtpClient();
                if (!string.IsNullOrWhiteSpace(config.Settings.Email.Password))
                    smtp.Credentials = new NetworkCredential(config.Settings.Email.UserName, config.Settings.Email.Password);
                smtp.EnableSsl = config.Settings.Email.EnableSsl;
                smtp.Host = config.Settings.Email.Host;
                if (config.Settings.Email.Port > 0)
                    smtp.Port = config.Settings.Email.Port;

                // Send message
                smtp.Send(msg);
                sent = true;
            }
            catch (Exception ex)
            {
            }

            return sent;
        }

        public void SetBaseUrl(IHttpContextAccessor Context)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(AppDomain.CurrentDomain.GetData("BaseUrl")?.ToString()))
                {
                    //string url = string.Format("{0}://{1}{2}", Context.HttpContext.Request.Scheme, Context.HttpContext.Request.Host, Context.HttpContext.Request.Path);
                    string url = string.Format("{0}://{1}{2}{3}", Context.HttpContext.Request.Scheme, Context.HttpContext.Request.Host, Context.HttpContext.Request.PathBase, Context.HttpContext.Request.Path);
                    Match match = Regex.Match(url, @"\/v\d+\/");

                    if (match.Success)
                    {
                        url = url.Substring(0, match.Index + 1);
                        AppDomain.CurrentDomain.SetData("BaseUrl", url);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public bool UpdateUserLanguage(HttpContext Context, UserInternal User)
        {
            return UpdateUserLanguage(Context, User, out UserInternal user);
        }

        public bool UpdateUserLanguage(HttpContext Context, UserInternal User, out UserInternal ModifiedUser)
        {
            bool updated = false;

            try
            {
                // Get language
                string lang = null;
                if (string.IsNullOrWhiteSpace(User.Language))
                    lang = GetCurrentLanguage(Context);
                else
                {
                    string lang2 = null;
                    if (!string.IsNullOrWhiteSpace(lang2 = GetHeaderLanguage(Context)) && User.Language != lang2)
                        lang = lang2;
                    else
                        lang = null;
                }

                if (!string.IsNullOrWhiteSpace(lang))
                {
                    // Update language
                    User.Language = lang;
                    updated = BL.User.Update(User);
                }
                else
                    updated = true;
            }
            catch (Exception ex)
            {
            }

            // Set modified user
            ModifiedUser = User;

            return updated;
        }
        #endregion
    }

    public static class ApiHelperExtensions
    {
        #region Object extensions
        public static T ConvertTo<T>(this object Object, Dictionary<string, string> RenameProperties = null)
        {
            if (Object == null)
                return default(T);
            else
            {
                string json = JsonConvert.SerializeObject(Object);

                if (RenameProperties != null && RenameProperties.Count > 0)
                    foreach (var kvp in RenameProperties)
                        json = json.Replace("\"" + kvp.Key + "\"", "\"" + kvp.Value + "\"");

                return JsonConvert.DeserializeObject<T>(json);
            }
        }
        #endregion
    }
}