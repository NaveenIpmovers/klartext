﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OtrohetsakutenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OtrohetsakutenApi.Helpers
{
    public class SwaggerHelper
    {
        #region Public methods
        public static void Filters(SwaggerGenOptions Options)
        {
            // Document
            Options.DocumentFilter<ApiVersion>();
            Options.DocumentFilter<EndpointSort>();
            Options.DocumentFilter<EnumDescription>();

            // Operation
            Options.OperationFilter<AcceptLanguage>();
            Options.OperationFilter<ApiVersionParameter>();
            Options.OperationFilter<AuthenticationRoles>();
            Options.OperationFilter<AuthorizationRequirement>();
            Options.OperationFilter<Responses>();
        }
        #endregion

        #region Classes
        public class AcceptLanguage : IOperationFilter
        {
            private AppSettings appSettings = null;

            public AcceptLanguage(IConfiguration configuration)
            {
                IConfigurationSection appSettingsSection = configuration.GetSection("AppSettings");
                appSettings = appSettingsSection.Get<AppSettings>();
            }

            public void Apply(Operation operation, OperationFilterContext context)
            {
                operation.Parameters.Add(new NonBodyParameter
                {
                    Name = "Accept-Language",
                    In = "header",
                    Description = "Accepted languages: " + string.Join(", ", appSettings.Language.Available),
                    Required = false,
                    Type = "string"
                });
            }
        }

        public class ApiVersion : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
            {
                swaggerDoc.Paths = swaggerDoc.Paths.ToDictionary(
                    path => path.Key.Replace("v{version}", swaggerDoc.Info.Version),
                    path => path.Value
                );
            }
        }

        public class ApiVersionParameter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                var versionParameter = operation.Parameters.FirstOrDefault(p => p.Name == "version");
                if (versionParameter != null)
                    operation.Parameters.Remove(versionParameter);
            }
        }

        public class AuthenticationRoles : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                bool authorization = false;
                CustomAttributeData attr = null;

                if ((attr = context.MethodInfo.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(AuthorizeAttribute))) != null)
                    authorization = true;
                else if ((attr = context.MethodInfo.ReflectedType.CustomAttributes.FirstOrDefault(x => x.AttributeType == typeof(AuthorizeAttribute))) != null && !context.MethodInfo.CustomAttributes.Any(x => x.AttributeType == typeof(AllowAnonymousAttribute)))
                    authorization = true;

                if (authorization)
                {
                    List<string> roles = new List<string>();
                    if (attr.NamedArguments != null && attr.NamedArguments.Count > 0 && attr.NamedArguments.Any(x => x.MemberName == "Roles"))
                        roles = attr.NamedArguments.FirstOrDefault(x => x.MemberName == "Roles").TypedValue.Value.ToString().Split(',').Select(x => x.Trim()).Distinct().OrderBy(x => x).ToList();

                    if (!roles.Any())
                        roles = Enum.GetNames(typeof(User.UserType)).Where(x => x != User.UserType.Unknown.ToString()).OrderBy(x => x).ToList();

                    if (roles.Any())
                        operation.Description += "<b>Authentication roles:</b> " + string.Join(", ", roles);
                }
            }
        }

        public class AuthorizationRequirement : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                bool authorization = false;

                if (context.MethodInfo.CustomAttributes.Any(x => x.AttributeType == typeof(AuthorizeAttribute)))
                    authorization = true;
                else if (context.MethodInfo.ReflectedType.CustomAttributes.Any(x => x.AttributeType == typeof(AuthorizeAttribute)) && !context.MethodInfo.CustomAttributes.Any(x => x.AttributeType == typeof(AllowAnonymousAttribute)))
                    authorization = true;

                if (authorization)
                    operation.Security = new List<IDictionary<string, IEnumerable<string>>>
                    {
                        new Dictionary<string, IEnumerable<string>>
                        {
                            { "Bearer", Array.Empty<string>() }
                        }
                    };
            }
        }

        public class EndpointSort : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
            {
                string[] httpMethods = new string[] { "get", "post", "put", "delete" };
                var paths = swaggerDoc.Paths.OrderBy(x => x.Key).ThenBy(x => Array.IndexOf(httpMethods, getMethod(x.Value))).ToDictionary(x => x.Key, x => x.Value);
                swaggerDoc.Paths = paths;
            }

            private string getMethod(PathItem pathItem)
            {
                if (pathItem.Delete != null)
                    return "delete";
                else if (pathItem.Get != null)
                    return "get";
                else if (pathItem.Post != null)
                    return "post";
                else if (pathItem.Put != null)
                    return "put";
                else
                    return "";
            }
        }

        public class EnumDescription : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, DocumentFilterContext context)
            {
                // add enum descriptions to result models
                foreach (var schemaDictionaryItem in swaggerDoc.Definitions)
                {
                    var schema = schemaDictionaryItem.Value;
                    foreach (var propertyDictionaryItem in schema.Properties)
                    {
                        var property = propertyDictionaryItem.Value;
                        var propertyEnums = property.Enum;
                        if (propertyEnums != null && propertyEnums.Count > 0)
                        {
                            property.Description += DescribeEnum(propertyEnums);
                        }
                    }
                }

                if (swaggerDoc.Paths.Count <= 0) return;

                // add enum descriptions to input parameters
                foreach (var pathItem in swaggerDoc.Paths.Values)
                {
                    DescribeEnumParameters(pathItem.Parameters);

                    // head, patch, options, delete left out
                    var possibleParameterisedOperations = new List<Operation> { pathItem.Get, pathItem.Post, pathItem.Put };
                    possibleParameterisedOperations.FindAll(x => x != null)
                        .ForEach(x => DescribeEnumParameters(x.Parameters));
                }
            }

            private static void DescribeEnumParameters(IList<IParameter> parameters)
            {
                if (parameters == null) return;

                foreach (var param in parameters)
                {
                    if (param.Extensions.ContainsKey("enum") && param.Extensions["enum"] is IList<object> paramEnums &&
                        paramEnums.Count > 0)
                    {
                        param.Description += DescribeEnum(paramEnums);
                    }
                }
            }

            private static string DescribeEnum(IEnumerable<object> enums)
            {
                var enumDescriptions = new List<string>();
                Type type = null;
                foreach (var enumOption in enums)
                {
                    if (type == null) type = enumOption.GetType();
                    enumDescriptions.Add($"{Convert.ChangeType(enumOption, type.GetEnumUnderlyingType())} = {Enum.GetName(type, enumOption)}");
                }

                return $"{Environment.NewLine}{string.Join(Environment.NewLine, enumDescriptions)}";
            }
        }

        public class Responses : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                bool authorization = false;

                if (context.MethodInfo.CustomAttributes.Any(x => x.AttributeType == typeof(AuthorizeAttribute)))
                    authorization = true;
                else if (context.MethodInfo.ReflectedType.CustomAttributes.Any(x => x.AttributeType == typeof(AuthorizeAttribute)) && !context.MethodInfo.CustomAttributes.Any(x => x.AttributeType == typeof(AllowAnonymousAttribute)))
                    authorization = true;

                if (authorization)
                {
                    if (!operation.Responses.Keys.Contains("401"))
                        operation.Responses.Add("401", new Response { Description = "Unauthorized" });
                    if (!operation.Responses.Keys.Contains("403"))
                        operation.Responses.Add("403", new Response { Description = "Forbidden" });
                }
                
                foreach (var kvp in operation.Responses.Where(x => x.Key == "302" && x.Value?.Description == "Redirect"))
                    kvp.Value.Description = "Found";

                if (!operation.Responses.Keys.Contains("500"))
                    operation.Responses.Add("500", new Response { Description = "Internal Server Error" });

                foreach (var kvp in operation.Responses.Where(x => x.Key == "502" && x.Value?.Description == "Server Error"))
                    kvp.Value.Description = "Bad Gateway (Third party error)";
            }
        }
        #endregion
    }
}