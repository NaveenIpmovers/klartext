﻿using System.Collections.Generic;

namespace OtrohetsakutenApi.Models
{
    public class Notification
    {
        #region Enums
        public enum NotificationPriority : int
        {
            High = 1,
            Critical = 2,
            Normal = 0
        }
        #endregion

        #region Properties
        public int Badge { get; set; }
        public Dictionary<string, string> Data { get; set; }
        public string Message { get; set; }
        public NotificationPriority Priority { get; set; }
        public string Title { get; set; }
        public int UserId { get; set; }
        #endregion
    }
}