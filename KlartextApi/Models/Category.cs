﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Category
    {
        #region Properties
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public long Id { get; set; }
        /// <summary>
        /// Category Name 
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        ///Faq List
        /// </summary>
        [DataMember(Name = "faqList")]
        public List<Faq> FaqList{ get; set; }
        #endregion
    }

    [DataContract]
    public class CategoryAdd
    {
        #region Properties
        /// <summary>
        /// Category Name in English
        /// </summary>
        [DataMember(Name = "name_enUS")]
        public string Name_enUS { get; set; }
        /// <summary>
        /// Category Name in Swedish
        /// </summary>
        [DataMember(Name = "name_svSE")]
        public string Name_svSE { get; set; }
        #endregion
    }
    public class CategoryInternal
    {
        #region Properties
        public long Id { get; set; }
        public string Name_enUS { get; set; }
        public string Name_svSE { get; set; }
        #endregion
    }

    [DataContract]
    public class CategoryUpdate 
    {
        /// <summary>
        /// Category Name in English
        /// </summary>
        [DataMember(Name = "name_enUS")]
        public string Name_enUS { get; set; }
        /// <summary>
        /// Category Name in Swedish
        /// </summary>
        [DataMember(Name = "name_svSE")]
        public string Name_svSE { get; set; }
    }
}
