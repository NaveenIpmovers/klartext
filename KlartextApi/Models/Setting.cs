﻿using System;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Setting
    {
        #region Properties
        /// <summary>
        /// Id of access token
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Access token
        /// </summary>
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Date created (UTC)
        /// </summary>
        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        #endregion
    }
}