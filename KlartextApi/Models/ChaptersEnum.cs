﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.Models
{
    public class ChapterEnum
    {
        #region Module Type
        /// <summary>
        /// Enum for type of Data module
        /// </summary>
        public enum ModuleType : int
        {
            /// <summary>
            /// Text=1(Data is of Text type)
            /// </summary>
            Text = 1,
            /// <summary>
            /// ExpandableInfo=1(Data is of Expandable type)
            /// </summary>
            ExpandableInfo = 2,
            /// <summary>
            /// TextwithInput=1(Data is of Text and Input Type)
            /// </summary>
            TextwithInput = 3,
            /// <summary>
            /// MultiselectCheckbox=1(Data is Multi select checkbox)
            /// </summary>
            MultiselectCheckbox = 4,
            /// <summary>
            /// SingleselectCheckbox=1(Data is Multi select checkbox)
            /// </summary>
            SingleselectCheckbox = 5
        }
        #endregion
    }
}
