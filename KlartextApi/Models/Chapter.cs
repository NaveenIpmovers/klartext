﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Chapter
    {
        #region Properties
        /// <summary>
        ///ID of chapter
        /// </summary>
        [DataMember(Name = "id")]
        public Int64 Id { get; set; }
        /// <summary>
        /// OverviewId
        /// </summary>
        [DataMember(Name = "overviewid")]
        public int OverviewId { get; set; }
        /// <summary>
        ///Description of chapter
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Image path of chapter
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        ///List of parts info
        /// </summary>
        [DataMember(Name = "parts")]
        public Paged<ChapterPart> Parts { get; set; }
        /// <summary>
        /// Status of chapter
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of chapter
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        #endregion
    }

    [DataContract]
    public class ChapterAdd
    {
        #region Properties
        /// <summary>
        /// OverviewId
        /// </summary>
        [DataMember(Name = "overviewid")]
        public int OverviewId { get; set; }
        /// <summary>
        /// Description of chapter in English
        /// </summary>
        [DataMember(Name = "description_enus")]
        public string Description_enUS { get; set; }
        /// <summary>
        /// Description of chapter in Swedish
        /// </summary>
        [DataMember(Name = "description_svse")]
        public string Description_svSE { get; set; }
        /// <summary>
        /// ImagePath of chapter
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        public IFormFile Image { get; set; }

        /// <summary>
        /// Status of chapter
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of chapter
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        #endregion
    }
    public class ChapterInternal
    {
        #region Properties
        public Int64 Id { get; set; }
        public int OverviewId { get; set; }
        public string Description_enUS { get; set; }
        public string Description_svSE { get; set; }
        public Byte[] Image { get; set; }
        public string ImageUrl{ get; set; }
        public bool Status { get; set; }
        public int SortOrder { get; set; }
        #endregion
    }

    [DataContract]
    public class ChapterUpdate 
    {
        /// <summary>
        /// OverviewId
        /// </summary>
        [DataMember(Name = "overviewid")]
        public int OverviewId { get; set; }
        /// <summary>
        /// Description of chapter in english
        /// </summary>
        [DataMember(Name = "description_enus")]
        public string Description_enUS { get; set; }
        /// <summary>
        /// Description of chapter in Swedish
        /// </summary>
        [DataMember(Name = "description_svse")]
        public string Description_svSE { get; set; }

        /// <summary>
        /// Image path of chapter
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        ///List of parts info
        /// </summary>
        [DataMember(Name = "parts")]
        public List<ChapterPart> Parts { get; set; }
        /// <summary>
        /// Status of chapter
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of chapter
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Image
        /// </summary>
        public IFormFile Image { get; set; }
    }
}
