﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChapterPartModuleItem
    {
        /// <summary>
        /// Subtitle
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Subtitle
        /// </summary>
        [DataMember(Name = "subtitle")]
        public string SubTitle { get; set; }
        /// <summary>
        /// Subtitle
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
    public class ChapterPartModuleItemInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        #endregion
    }
}
