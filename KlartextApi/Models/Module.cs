﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Module
    {
        #region Properties
        /// <summary>
        ///ID of Module
        /// </summary>
        [DataMember(Name = "id")]
        public Int64 Id { get; set; }
        /// <summary>
        ///Text of Module
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
     
        #endregion
    }
    public class ModuleInternal
    {
        #region Properties
        public Int64 Id { get; set; }
        public string Text { get; set; }

        #endregion
    }
    [DataContract]
    public class ModuleAdd: Module
    {
    }

    [DataContract]
    public class ModuleUpdate: Module
    {
       
    }
}
