﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Chat
    {
        #region Properties

        /// <summary>
        /// Chat Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// UserId
        /// </summary>
        [DataMember(Name = "userid")]
        public int UserId { get; set; }
        /// <summary>
        /// UserName
        /// </summary>
        [DataMember(Name = "username")]
        public string UserName { get; set; }
        /// <summary>
        /// ChannelId
        /// </summary>
        [DataMember(Name = "channelid")]
        public string ChannelId { get; set; }
        #endregion
    }

    [DataContract]
    public class ChatAdd
    {
        #region Properties
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }

        /// <summary>
        /// ChannelId
        /// </summary>
        [DataMember(Name = "channelid")]
        public string ChannelId { get; set; }
        #endregion
    }
    public class ChatInternal
    {
        #region Properties
        public Int64 Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public string ChannelId { get; set; }
        #endregion
    }

}
