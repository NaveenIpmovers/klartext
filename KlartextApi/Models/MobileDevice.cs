﻿using System.Linq;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class MobileDevice
    {
        #region Properties
        /// <summary>
        /// Device id of mobile device
        /// </summary>
        [DataMember(Name = "deviceId")]
        public string DeviceId { get; set; }

        /// <summary>
        /// Platform of mobile device
        /// Available: android, ios, windows
        /// </summary>
        [DataMember(Name = "platform")]
        public string Platform { get; set; }

        /// <summary>
        /// Notification service of mobile device
        /// Available: apns, fcm, gcm, mpns, wns
        /// </summary>
        [DataMember(Name = "service")]
        public string Service { get; set; }
        #endregion

        #region Public methods
        public bool IsValidPlatform()
        {
            return (!string.IsNullOrWhiteSpace(Platform) && new string[] { "android", "ios", "windows" }.Contains(Platform.ToLower()));
        }

        public bool IsValidService()
        {
            return (!string.IsNullOrWhiteSpace(Service) && new string[] { "apns", "fcm", "gcm", "mpns", "wns" }.Contains(Service.ToLower()));
        }
        #endregion
    }

    [DataContract]
    public class MobileDeviceDelete
    {
        #region Properties
        /// <summary>
        /// Device id of mobile device
        /// </summary>
        [DataMember(Name = "deviceId")]
        public string DeviceId { get; set; }

        /// <summary>
        /// Notification service of mobile device
        /// Available: apns, fcm, gcm, mpns, wns
        /// </summary>
        [DataMember(Name = "service")]
        public string Service { get; set; }
        #endregion

        #region Public methods
        public bool IsValidService()
        {
            return new MobileDevice { Service = Service }.IsValidService();
        }
        #endregion
    }
}