﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ValidationRegularExpressions
    {
        #region Properties
        /// <summary>
        /// Password regular expression
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Invalid password regular expression text
        /// </summary>
        [DataMember(Name = "passwordText")]
        public string PasswordText { get; set; }
        #endregion
    }
}