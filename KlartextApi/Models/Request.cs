﻿using System.Collections.Generic;

namespace ShoutawayApi.Models
{

    public class RequestImageString
    {
        #region Properties
        public string Value { get; set; }
        public int OrderId { get; set; }
        #endregion
    }



    public class RequestString
    {
        #region Properties
        public string Value { get; set; }
        #endregion
    }
}