﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class UserPayment
    {
        #region constants
        public static string IosPlatform = "IOS";
        public static string AndroidPlatform = "ANDROID";
        public static string ManualPlatform = "MANUAL";
        #endregion

        #region Properties
        /// <summary>
        /// User ID
        /// </summary>
        [DataMember(Name = "userid")]
        public int UserId { get; set; }
        /// <summary>
        /// In-App Purchase Id
        /// </summary>
        [DataMember(Name = "iapid")]
        public string IapId { get; set; }
        /// <summary>
        /// Receipt
        /// </summary>
        [DataMember(Name = "receipt")]
        public string Receipt { get; set; }
        /// <summary>
        /// Plateform
        /// </summary>
        [DataMember(Name = "platform")]
        public string Platform { get; set; }
        #endregion
    }

    [DataContract]
    public class UserPaymentAdd
    {
        #region Properties
        /// <summary>
        /// In-App Purchase Id
        /// </summary>
        [DataMember(Name = "iapid")]
        public string IapId { get; set; }
        /// <summary>
        /// Receipt
        /// </summary>
        [DataMember(Name = "receipt")]
        public string Receipt { get; set; }
        /// <summary>
        /// Plateform
        /// </summary>
        [DataMember(Name = "platform")]
        public string Platform { get; set; }
        #endregion
    }

    public class UserPaymentInternal
    {
        #region Properties
        public long Id { get; set; }
        public int UserId { get; set; }
        public string IapId { get; set; }
        public string Receipt { get; set; }
        public string Platform { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        #endregion
    }
    //user payment response
    public class UserPaymentResponse
    {
        public bool Success { get; set; }
        public bool Expired { get; set; }
        public string ResponseMessage { get; set; }
    }
}