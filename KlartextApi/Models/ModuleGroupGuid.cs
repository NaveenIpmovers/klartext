﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{

    [DataContract]
    public class ModuleGroupGuid
    {
        #region Properties
        /// <summary>
        /// Module Type Id
        /// </summary>
           [DataMember(Name = "moduletypeid")]
        public int ModuleTypeId { get; set; }

        /// <summary>
        ///Module Guid
        /// </summary>
        [DataMember(Name = "moduleguid")]
        public Guid ModuleGuid { get; set; }
        /// <summary>
        ///Module Group Text
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }

        #endregion
    }
    public class ModuleGroupGuidInternal
    {
        #region Properties
        public int ModuleTypeId { get; set; }
        public Guid ModuleGuid { get; set; }
        public string Title { get; set; }
        #endregion
    }
}
