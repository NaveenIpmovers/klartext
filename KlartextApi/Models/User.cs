﻿using System;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class User
    {
        #region Enums
        public enum UserType : int
        {
            Admin = 2,
            SuperAdmin = 3,
            User = 1,
            Unknown = 0
        }
        #endregion

        #region Properties
        /// <summary>
        /// Email of user
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// Email verified by user
        /// </summary>
        [DataMember(Name = "emailVerified")]
        public bool EmailVerified { get; set; }

        /// <summary>
        /// Id of user
        /// </summary>
        [DataMember(Name = "id")]
        public long Id { get; set; }

        /// <summary>
        /// Language of user
        /// </summary>
        [DataMember(Name = "language")]
        public string Language { get; set; }

        /// <summary>
        /// Type of user
        /// </summary>
        [DataMember(Name = "type")]
        public UserType Type { get; set; }

        /// <summary>
        /// Type of user
        /// </summary>
        [DataMember(Name = "typeAsText")]
        public string TypeAsText { get { return Type.ToString(); } }


        /// <summary>
        /// First name of user
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of user
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// User Name for chat
        /// </summary>
        [DataMember(Name = "userName")]
        public string UserName { get; set; }
        /// <summary>
        /// User Subscription
        /// </summary>
        [DataMember(Name = "isSubscribe")]
        public bool IsSubscribe { get; set; }

        /// <summary>
        ///  User Subscription PurchaseDate
        /// </summary>
        [DataMember(Name = "purchasedate")]
        public DateTime? PurchaseDate { get; set; }
        /// <summary>
        ///  User Subscription ExpiryDate
        /// </summary>
        [DataMember(Name = "expirydate")]
        public DateTime? ExpiryDate { get; set; }

        #endregion
    }

    [DataContract]
    public class UserAdd
    {
        #region Properties
        /// <summary>
        /// First name of user
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of user
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// User Name for chat
        /// </summary>
        [DataMember(Name = "userName")]
        public string UserName { get; set; }
        /// <summary>
        /// Email of user
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// Password of user
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }

        #endregion
    }

    public class UserInternal
    {
        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public bool EmailVerified { get; set; }
        public long Id { get; set; }
        public string Language { get; set; }
        public string Password { get; set; }
        public User.UserType Type { get; set; }
        public bool IsSubscribe { get; set; }
        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public bool Notifications { get; set; }
        #endregion
    }

    [DataContract]
    public class UserUpdate
    {
        #region Properties
        /// <summary>
        /// Email of user
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// First name of user
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name of user
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// User Name for chat
        /// </summary>
        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Password of user
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }

        /// <summary>
        /// Type of user
        /// </summary>
        [DataMember(Name = "type")]
        public User.UserType Type { get; set; }
        #endregion
    }
    [DataContract]
    public class AppUserUpdate
    {
        #region Properties
        /// <summary>
        /// User Name for chat
        /// </summary>
        [DataMember(Name = "userName")]
        public string UserName { get; set; }

        /// <summary>
        /// Password of user
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }
        /// <summary>
        /// Notifications status of app user
        /// </summary>
        [DataMember(Name = "notifications")]
        public bool? Notifications { get; set; }
        #endregion
    }
}