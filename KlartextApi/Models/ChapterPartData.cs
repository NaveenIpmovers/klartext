﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChapterPartData
    {

        #region Properties
        /// <summary>
        ///Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }
        /// <summary>
        /// subtitle
        /// </summary>
        [DataMember(Name = "subtitle")]
        public string SubTitle { get; set; }
        /// <summary>
        /// subtitle
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }
        /// <summary>
        /// option Text
        /// </summary>
        [DataMember(Name = "optiontext")]
        public string OptionText { get; set; }
        /// <summary>
        /// Type of module type
        /// </summary>
        [DataMember(Name = "moduletypeid")]
        public ChapterEnum.ModuleType ModuleTypeId { get; set; }
        /// <summary>
        /// Type of module type text
        /// </summary>
        [DataMember(Name = "typeAsText")]
        public string TypeAsText { get { return ModuleTypeId.ToString(); } }
        /// <summary>
        /// Image path of news
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageURL { get; set; }
        /// <summary>
        /// Status of Data
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort Order of Data
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Module Guid
        /// </summary>
        [DataMember(Name = "moduleguid")]
        public Guid Moduleguid { get; set; }
        /// <summary>
        /// Use Image
        /// </summary>
        [DataMember(Name = "useimage")]
        public bool UseImage { get; set; }
        /// <summary>
        /// Audio Url
        /// </summary>
        [DataMember(Name = "audiourl")]
        public string AudioUrl{ get; set; }

        #endregion
    }
    public class ChapterPartDataInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Title_enUS { get; set; }
        public string Title_svSE { get; set; }
        public string SubTitle_enUS { get; set; }
        public string SubTitle_svSE { get; set; }
        public string Description_enUS { get; set; }
        public string Description_svSE { get; set; }
        public string OptionText_enUS { get; set; }
        public string OptionText_svSE { get; set; }
        public ChapterEnum.ModuleType ModuleTypeId { get; set; }
        public string TypeAsText { get { return ModuleTypeId.ToString(); } }
        public Guid ModuleGuid { get; set; }
        public int SortOrder { get; set; }
        public bool Status { get; set; }
        public string ImageURL { get; set; }
        public bool UseImage { get; set; }
        public string AudioUrl_enUS { get; set; }
        public string AudioUrl_svSE { get; set; }
        #endregion
    }

    public class ChapterPartDataAdd 
    {
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }
        [DataMember(Name = "title_svsE")]
        public string Title_svSE { get; set; }
        [DataMember(Name = "subtitle_enus")]
        public string SubTitle_enUS { get; set; }
        [DataMember(Name = "subtitle_svse")]
        public string SubTitle_svSE { get; set; }
        [DataMember(Name = "description_enus")]
        public string Description_enUS { get; set; }
        [DataMember(Name = "description_svse")]
        public string Description_svSE { get; set; }
        [DataMember(Name = "optiontext_enus")]
        public string OptionText_enUS { get; set; }
        [DataMember(Name = "optiontext_svse")]
        public string OptionText_svSE { get; set; }

        [DataMember(Name = "moduleguid")]
        public Guid ModuleGuid { get; set; }
    }
    public class ChapterPartDataUpdate 
    {
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }
        [DataMember(Name = "title_svsE")]
        public string Title_svSE { get; set; }
        [DataMember(Name = "subtitle_enus")]
        public string SubTitle_enUS { get; set; }
        [DataMember(Name = "subtitle_svse")]
        public string SubTitle_svSE { get; set; }
        [DataMember(Name = "description_enus")]
        public string Description_enUS { get; set; }
        [DataMember(Name = "description_svse")]
        public string Description_svSE { get; set; }
        [DataMember(Name = "optiontext_enus")]
        public string OptionText_enUS { get; set; }
        [DataMember(Name = "optiontext_svse")]
        public string OptionText_svSE { get; set; }
    }
    public class UpdateAudio
    {
        /// <summary>
        /// Audio of chapter in English
        /// </summary>
        [DataMember(Name = "audiourl_enUS")]
        public string AudioUrl_enUS { get; set; }

        /// <summary>
        /// Audio of chapter in Swedish
        /// </summary>
        [DataMember(Name = "audiourl_svSE")]
        public string AudioUrl_svSE { get; set; }

        /// <summary>
        /// Audio in english
        /// </summary>
        public IFormFile Audio_enUS { get; set; }
        /// <summary>
        /// Audio in Swedish
        /// </summary>
        public IFormFile Audio_svSE { get; set; }
    }
}
