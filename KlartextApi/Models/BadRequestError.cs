﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class BadRequestError
    {
        #region Enums
        public enum ErrorLocation : int
        {
            Account = 5,
            Body = 3,
            Header = 4,
            Path = 1,
            QueryString = 2,
            Unknown = 0
        }

        public enum ErrorMessage : int
        {
            Invalid = 2,
            LengthTooShort = 6,
            NotNumeric = 5,
            NotVerified = 7,
            OutOfRange = 4,
            Required = 1,
            Unknown = 0,
            Verified = 3
        }
        #endregion

        #region Properties
        [DataMember(Name = "error")]
        public string Error { get; set; }

        [DataMember(Name = "location")]
        public ErrorLocation Location { get; set; }

        [DataMember(Name = "locationAsText")]
        public string LocationAsText { get { return Location.ToString(); } }

        [DataMember(Name = "message")]
        public ErrorMessage Message { get; set; }

        [DataMember(Name = "messageAsText")]
        public string MessageAsText { get { return Message.ToString(); } }
        #endregion
    }
}