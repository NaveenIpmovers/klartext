﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChatStatus
    {
        #region Properties

        /// <summary>
        /// Status Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Start Time of Chat
        /// </summary>
        [DataMember(Name = "starttime")]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Start Time of Chat
        /// </summary>
        [DataMember(Name = "endtime")]
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Chat Status
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }

        /// <summary>
        /// Manual Open
        /// </summary>
        [DataMember(Name = "manualopen")]
        public bool ManualOpen { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DataMember(Name = "textMessage")]
        public string TextMessage { get; set; }
        #endregion
    }

    public class ChatStatusInternal
    {
        #region Properties
        public Int64 Id { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public string StartTimeStr { get; set; }
        public string EndTimeStr { get; set; }
        public bool Status { get; set; }
        public bool ManualOpen { get; set; }
        public string TextMessage_enUS { get; set; }
        public string TextMessage_svSE { get; set; }
        #endregion
    }

    [DataContract]
    public class ChatStatusUpdate
    {
        /// <summary>
        /// Chat Status Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Chat Status Manual Open 
        /// </summary>
        [DataMember(Name = "manualopen")]
        public bool ManualOpen { get; set; }
        /// <summary>
        /// Text Message in english
        /// </summary>
        [DataMember(Name = "textMessage_enus")]
        public string TextMessage_enUS { get; set; }

        /// <summary>
        /// Text Message in swedish
        /// </summary>
        [DataMember(Name = "textMessage_svse")]
        public string TextMessage_svSE { get; set; }

    }
}
