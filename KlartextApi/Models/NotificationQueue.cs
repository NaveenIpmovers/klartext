﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class NotificationQueue
    {
        #region Properties
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public long Id { get; set; }
        /// <summary>
        /// title of message
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }
        /// <summary>
        /// Message of notification
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }

        #endregion
    }

    [DataContract]
    public class NotificationQueueAdd
    {
        #region Properties
        /// <summary>
        /// Title in english
        /// </summary>
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }
        /// <summary>
        /// Title in swedish
        /// </summary>
        [DataMember(Name = "title_svse")]
        public string Title_svSE { get; set; }
        /// <summary>
        /// Message in english
        /// </summary>
        [DataMember(Name = "message_enus")]
        public string Message_enUS { get; set; }
        /// <summary>
        /// Message in swedish
        /// </summary>
        [DataMember(Name = "message_svse")]
        public string Message_svSE { get; set; }

        #endregion
    }
    public class NotificationQueueInternal
    {
        #region Properties
        public long Id { get; set; }
        public string Title_enUS { get; set; }
        public string Title_svSE { get; set; }
        public string Message_enUS { get; set; }
        public string Message_svSE { get; set; }
        public long UserId { get; set; }
     
        #endregion
    }

    [DataContract]
    public class NotificationQueueUpdate: NotificationQueue
    {
        #region Properties
        /// <summary>
        /// Title in english
        /// </summary>
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }
        /// <summary>
        /// Title in swedish
        /// </summary>
        [DataMember(Name = "title_svse")]
        public string Title_svSE { get; set; }
        /// <summary>
        /// Message in english
        /// </summary>
        [DataMember(Name = "message_enus")]
        public string Message_enUS { get; set; }
        /// <summary>
        /// Message in swedish
        /// </summary>
        [DataMember(Name = "message_svse")]
        public string Message_svSE { get; set; }

        #endregion
    }
}