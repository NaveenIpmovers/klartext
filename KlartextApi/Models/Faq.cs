﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Faq
    {
        #region Properties
        /// <summary>
        ///  Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Category Id
        /// </summary>
        [DataMember(Name = "categoryId")]
        public int CategoryId { get; set; }
        /// <summary>
        ///Question 
        /// </summary>
        [DataMember(Name = "question")]
        public string Question { get; set; }
        /// <summary>
        ///Answer 
        /// </summary>
        [DataMember(Name = "answer")]
        public string Answer { get; set; }
        /// <summary>
        /// Status of faq
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of faq
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Show Answer If User is Logged-In
        /// </summary>
        [DataMember(Name = "showAnswerIfLogin")]
        public bool ShowAnswerIfLogin { get; set; }
        /// <summary>
        /// Show Answer If User is Subscribed
        /// </summary>
        [DataMember(Name = "showAnswerIfSubscribed")]
        public bool ShowAnswerIfSubscribed { get; set; }

        #endregion
    }

    [DataContract]
    public class FaqAdd
    {
        #region Properties
        /// <summary>
        /// Category Id
        /// </summary>
        [DataMember(Name = "categoryId")]
        public int CategoryId { get; set; }
        /// <summary>
        ///Question in English
        /// </summary>
        [DataMember(Name = "question_enus")]
        public string Question_enUS { get; set; }
        /// <summary>
        ///Question in English
        /// </summary>
        [DataMember(Name = "question_svse")]
        public string Question_svSE { get; set; }
        /// <summary>
        ///Answer in English
        /// </summary>
        [DataMember(Name = "answer_enus")]
        public string Answer_enUS { get; set; }
        /// <summary>
        ///Answer in Swedish
        /// </summary>
        [DataMember(Name = "answer_svse")]
        public string Answer_svSE { get; set; }
        /// <summary>
        /// Status of faq
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of faq
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Show Answer If User is Logged-In
        /// </summary>
        [DataMember(Name = "showAnswerIfLogin")]
        public bool ShowAnswerIfLogin { get; set; }
        /// <summary>
        /// Show Answer If User is Subscribed
        /// </summary>
        [DataMember(Name = "showAnswerIfSubscribed")]
        public bool ShowAnswerIfSubscribed { get; set; }
        #endregion
    }
    public class FaqInternal
    {
        #region Properties
        public long Id { get; set; }
        public int CategoryId { get; set; }
        public string Question_enUS { get; set; }
        public string Question_svSE { get; set; }
        public string Answer_enUS { get; set; }
        public string Answer_svSE { get; set; }
        public bool Status { get; set; }
        public int SortOrder { get; set; }
        public bool ShowAnswerIfLogin { get; set; }
        public bool ShowAnswerIfSubscribed { get; set; }
        #endregion
    }

    [DataContract]
    public class FaqUpdate
    {
        /// <summary>
        ///Question in English
        /// </summary>
        [DataMember(Name = "question_enus")]
        public string Question_enUS { get; set; }
        /// <summary>
        ///Question in English
        /// </summary>
        [DataMember(Name = "question_svse")]
        public string Question_svSE { get; set; }
        /// <summary>
        ///Answer in English
        /// </summary>
        [DataMember(Name = "answer_enus")]
        public string Answer_enUS { get; set; }
        /// <summary>
        ///Answer in Swedish
        /// </summary>
        [DataMember(Name = "answer_svse")]
        public string Answer_svSE { get; set; }
        /// <summary>
        /// Status of faq
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of faq
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Show Answer If User is Logged-In
        /// </summary>
        [DataMember(Name = "showAnswerIfLogin")]
        public bool ShowAnswerIfLogin { get; set; }
        /// <summary>
        /// Show Answer If User is Subscribed
        /// </summary>
        [DataMember(Name = "showAnswerIfSubscribed")]
        public bool ShowAnswerIfSubscribed { get; set; }
    }

}
