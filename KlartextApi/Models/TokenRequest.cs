﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class TokenRequest
    {
        #region Properties
        /// <summary>
        /// Refresh token
        /// </summary>
        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }
        #endregion
    }
}