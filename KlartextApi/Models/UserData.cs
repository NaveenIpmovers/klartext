﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class UserData
    {
        #region Properties
        /// <summary>
        /// Module ID
        /// </summary>
        [DataMember(Name = "moduleguid")]
        public Guid ModuleGuid { get; set; }
        /// <summary>
        /// Module Type Id
        /// </summary>
        [DataMember(Name = "moduletypeid")]
        public int ModuleTypeId { get; set; }
        /// <summary>
        /// User Data
        /// </summary>
        [DataMember(Name = "data")]
        public string Data { get; set; }
        #endregion
    }

    [DataContract]
    public class UserDataAdd
    {
        #region Properties
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember(Name = "chapterpartid")]
        public int ChapterPartId;
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember(Name = "modules")]
        public List<UserData> modules;
        #endregion
    }

    public class UserDataInternal
    {
        #region Properties
        public long Id { get; set; }
        public long UserId { get; set; }
        public int ChapterPartId { get; set; }
        public Guid ModuleGuid { get; set; }
        public int ModuleTypeId { get; set; }
        public string Data { get; set; }
        public bool Done { get; set; }

        #endregion
    }
}