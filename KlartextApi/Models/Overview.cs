﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Overview
    {
        #region Properties
        /// <summary>
        ///ID of chapter
        /// </summary>
        [DataMember(Name = "id")]
        public Int64 Id { get; set; }
        
        /// <summary>
        ///Description of Overview
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        ///List of Chapter info
        /// </summary>
        [DataMember(Name = "chapters")]
        public List<Chapter> chapters { get; set; }

        #endregion
    }

    [DataContract]
    public class OverviewAdd
    {
        #region Properties
        /// <summary>
        /// Description of chapter
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// ImagePath of chapter
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        public IFormFile Image { get; set; }

        /// <summary>
        /// Status of chapter
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of chapter
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        #endregion
    }
    public class OverviewInternal
    {
        #region Properties
        public Int64 Id { get; set; }
        public string Description_enUS { get; set; }
        public string Description_svSE { get; set; }
        public List<Chapter> chapters { get; set; }
        #endregion
    }

    [DataContract]
    public class OverviewUpdate
    {
        /// <summary>
        /// Description of chapter
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// Image path of chapter
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }

        /// <summary>
        ///List of parts info
        /// </summary>
        [DataMember(Name = "parts")]
        public List<ChapterPart> Parts { get; set; }
        /// <summary>
        /// Status of chapter
        /// </summary>
        [DataMember(Name = "status")]
        public bool Status { get; set; }
        /// <summary>
        /// Sort order of chapter
        /// </summary>
        [DataMember(Name = "sortorder")]
        public int SortOrder { get; set; }
        /// <summary>
        /// Image
        /// </summary>
    }
}
