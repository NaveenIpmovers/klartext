﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class EmailRequest
    {
        #region Properties
        /// <summary>
        /// Email to request password for
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }
        #endregion
    }
}