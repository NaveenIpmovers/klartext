﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class UserAuth
    {
        #region Properties
        /// <summary>
        /// User email
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [DataMember(Name = "password")]
        public string Password { get; set; }
        #endregion
    }
}