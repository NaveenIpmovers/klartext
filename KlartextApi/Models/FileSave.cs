﻿using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class FileSave
    {
        #region Properties
        /// <summary>
        /// Driver Name
        /// </summary>
        [DataMember(Name = "driverName")]
        public string DriverName { get; set; }

        /// <summary>
        /// File Name
        /// </summary>
        [DataMember(Name = "fileName")]
        public string FileName { get; set; }

        /// <summary>
        /// Message
        /// </summary>
        [DataMember(Name = "message")]
        public string Message { get; set; }
        #endregion
    }
}