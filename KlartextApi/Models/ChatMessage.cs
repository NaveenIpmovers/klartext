﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChatMessage
    {
        #region Properties
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "Id")]
        public Int64 Id { get; set; }
        /// <summary>
        /// Chat Id
        /// </summary>
        [DataMember(Name = "chatId")]
        public int ChatId { get; set; }
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember(Name = "userId")]
        public int UserId { get; set; }
        /// <summary>
        /// User Name
        /// </summary>
        [DataMember(Name = "userName")]
        public string UserName { get; set; }
        /// <summary>
        ///Text of Chat
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }

        /// <summary>
        /// Chat Create Date
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        #endregion
    }

    [DataContract]
    public class ChatMessageAdd
    {
        #region Properties
        /// <summary>
        /// Chat Id
        /// </summary>
        [DataMember(Name = "chatId")]
        public int ChatId { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        #endregion
    }
    public class ChatMessageInternal
    {
        #region Properties
        public int Id { get; set; }
        public int ChatId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        public DateTime CreateDate { get; set; }
        #endregion
    }

}
