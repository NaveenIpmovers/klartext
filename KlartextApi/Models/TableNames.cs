﻿namespace OtrohetsakutenApi.Models
{
    public class TableNames
    {
        public readonly string RefreshTokens = "refreshtokens";
        public readonly string Users = "users";
        public readonly string Chapter = "chapter";
        public readonly string ChapterPart = "chapter_part";
        public readonly string ChapterPartData = "chapter_part_data";
        public readonly string UserData = "user_data";
        public readonly string Module = "module_type";
        public readonly string Overview = "overview";
        public readonly string Faq = "faq";
        public readonly string UserPayment = "user_payment";
        public readonly string Setting = "setting";
        public readonly string Chat = "chat";
        public readonly string ChatMessage = "chatmessage";
        public readonly string ChatStatus= "chatstatus";
        public readonly string Category = "category";
        public readonly string NotificationQueue = "notification_queue";
    }
}