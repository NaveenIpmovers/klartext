﻿namespace OtrohetsakutenApi.Models
{
    public class AppSettings
    {
        #region Properties
        public EmailSection Email { get; set; }
        public JwtSection Jwt { get; set; }
        public LanguageSection Language { get; set; }
        public OwnerSection Owner { get; set; }
        public ProjectSection Project { get; set; }
        public SqlSection Sql { get; set; }
        public InAppPurchaseSection InAppPurchase { get; set; }
        public FaqSetting faqSetting { get; set; }

        public NotificationSection Notification { get; set; }
        #endregion

        public class EmailSection
        {
            #region Properties
            public bool EnableSsl { get; set; }
            public string Host { get; set; }
            public string Password { get; set; }
            public int Port { get; set; }
            public string Sender { get; set; }
            public string SenderName { get; set; }
            public string UserName { get; set; }
            #endregion
        }

        public class JwtSection
        {
            #region Properties
            public string Secret { get; set; }
            #endregion
        }

        public class LanguageSection
        {
            #region Properties
            public string[] Available { get; set; }
            public string Default { get; set; }
            #endregion
        }

        public class OwnerSection
        {
            #region Properties
            public string Name { get; set; }
            public string NameHTML { get; set; }
            public string PossessionName { get; set; }
            public string PossessionNameHTML { get; set; }
            #endregion
        }

        public class ProjectSection
        {
            #region Properties
            public string AppWebBaseUrl { get; set; }
            public int PageLimitMax { get; set; }
            public string AppBaseUrl { get; set; }
            #endregion
        }

        public class SqlSection
        {
            #region Properties
            public string ConnectionString { get; set; }
            #endregion
        }

        public class InAppPurchaseSection
        {
            #region Properties
            public bool Sandbox { get; set; }
            #endregion
        }
        public class FaqSetting
        {
            #region Properties
            public bool showAnswerIfLogin { get; set; }
            public bool showAnswerIfSubscribed { get; set; }
            #endregion
        }
        public class NotificationSection
        {
            #region Properties
            public string BaseUrl { get; set; }
            public string Key { get; set; }
            #endregion
        }
    }
}