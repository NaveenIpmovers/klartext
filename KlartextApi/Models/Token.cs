﻿using System;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Token
    {
        #region Properties
        /// <summary>
        /// Access token
        /// </summary>
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Date created (UTC)
        /// </summary>
        [DataMember(Name = "date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Expiration in seconds
        /// </summary>
        [DataMember(Name = "expiresIn")]
        public int ExpiresIn { get; set; }

        /// <summary>
        /// Refresh token needed to obtain new access token when expired
        /// </summary>
        [DataMember(Name = "refreshToken")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// Type of access token
        /// </summary>
        [DataMember(Name = "tokenType")]
        public string TokenType { get; set; } = "Bearer";

        /// <summary>
        /// User id
        /// </summary>
        [DataMember(Name = "userId")]
        public long UserId { get; set; }

        /// <summary>
        /// User type
        /// </summary>
        [DataMember(Name = "userType")]
        public User.UserType UserType { get; set; }

        /// <summary>
        /// User type
        /// </summary>
        [DataMember(Name = "userTypeAsText")]
        public string UserTypeAsText { get { return UserType.ToString(); } }
        #endregion
    }
}