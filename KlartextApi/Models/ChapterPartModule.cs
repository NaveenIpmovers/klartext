﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChapterPartModule
    {
        /// <summary>
        /// Module Type
        /// </summary>
        [DataMember(Name = "moduletypeid")]
        public ChapterEnum.ModuleType ModuleTypeId { get; set; }
        /// <summary>
        /// Module Type as text
        /// </summary>
        [DataMember(Name = "typeAsText")]
        public string TypeAsText { get { return ModuleTypeId.ToString(); } }
        /// <summary>
        /// Module Data
        /// </summary>
        [DataMember(Name = "moduledata")]
        public dynamic ModuleData { get; set; }
        /// <summary>
        /// ModuleGUID
        /// </summary>
        [DataMember(Name = "moduleguid")]
        public Guid ModuleGuid { get; set; }
        /// <summary>
        /// sortOrder
        /// </summary>
        [DataMember(Name = "sortOrder")]

        public int sortorder { get; set; }
    }
    public class ChapterPartModuleInternal
    {
        #region Properties
        public int sortorder { get; set; }
        public ChapterEnum.ModuleType ModuleTypeId { get; set; }
        public string TypeAsText { get { return ModuleTypeId.ToString(); } }
        public dynamic ModuleData { get; set; }
        public Guid ModuleGuid { get; set; }
        #endregion
    }
    [DataContract]
    public class ChPartModuleDataText
    {
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }
        /// <summary>
        /// Audio URL in english
        /// </summary>
        [DataMember(Name = "audioUrl")]
        public string AudioUrl{ get; set; }

    }
    public class ChPartModuleDataTextInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string AudioUrl { get; set; }
        #endregion
    }
    [DataContract]
    public class ChPartModuleDataTextWithInput
    {
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }
        /// <summary>
        /// input
        /// </summary>
        [DataMember(Name = "input")]
        public string Input { get; set; }

    }
    public class ChPartModuleDataTextWithInputInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public string Input { get; set; }
        #endregion
    }

    [DataContract]
    public class ChPartModuleDataCheckbox
    {
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "options")]
        public List<CheckboxOption> options { get; set; }
        /// <summary>
        /// Selected Options
        /// </summary>
        [DataMember(Name = "selectedoptions")]
        public string SelectedOptions { get; set; }

    }
    public class ChPartModuleDataCheckboxInternal
    {
        #region Properties
        public string Text { get; set; }
        public List<CheckboxOption> options { get; set; }
        #endregion
    }
    [DataContract]
    public class CheckboxOption
    {
        #region Properties
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        #endregion
    }
    public class CheckboxOptionInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        #endregion
    }
    [DataContract]
    public class ChPartModuleDataExpandable
    {
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        /// <summary>
        /// Items
        /// </summary>
        [DataMember(Name = "items")]
        public List<ExpandableItems> items { get; set; }
    }
    public class ChPartModuleDataExpandableInternal
    {
        #region Properties
        public string Text { get; set; }
        public List<ExpandableItems> items { get; set; }
        #endregion
    }
    [DataContract]
    public class ExpandableItems
    {
        #region Properties
        /// <summary>
        /// Id
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }
        /// <summary>
        /// Text
        /// </summary>
        [DataMember(Name = "text")]
        public string Text { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// ImageUrl
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }
        #endregion
    }
    public class ExpandableItemsInternal
    {
        #region Properties
        public int Id { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        #endregion
    }
}