﻿using System;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class  RefreshToken
    {
        #region Properties
        /// <summary>
        /// Access token
        /// </summary>
        [DataMember(Name = "accessToken")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Id of refresh token
        /// </summary>
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Refresh token
        /// </summary>
        [DataMember(Name = "token")]
        public string Token { get; set; }

        /// <summary>
        /// Owner of refresh token
        /// </summary>
        [DataMember(Name = "userId")]
        public long UserId { get; set; }
        #endregion
    }
}