﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class Paged<T>
    {
        #region Properties
        /// <summary>
        /// Number of items for current page
        /// </summary>
        [DataMember(Name = "items")]
        public int Items { get; set; }

        /// <summary>
        /// List of objects for current page
        /// </summary>
        [DataMember(Name = "list")]
        public List<T> List { get; set; }

        /// <summary>
        /// Current page index
        /// </summary>
        [DataMember(Name = "pageIndex")]
        public int PageIndex { get; set; }

        /// <summary>
        /// Total number of items
        /// </summary>
        [DataMember(Name = "totalItems")]
        public int TotalItems { get; set; }

        /// <summary>
        /// Total number of pages
        /// </summary>
        [DataMember(Name = "totalPages")]
        public int TotalPages { get; set; }
        #endregion
    }
}