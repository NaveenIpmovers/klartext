﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace OtrohetsakutenApi.Models
{
    [DataContract]
    public class ChapterPart
    {
        #region Properties
        /// <summary>
        /// Chapter Part ID
        /// </summary>
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Chapter Id
        /// </summary>
        [DataMember(Name = "chapterId")]
        public int ChapterId { get; set; }

        /// <summary>
        ///Title
        /// </summary>
        [DataMember(Name = "title")]
        public string Title { get; set; }
        /// <summary>
        ///Part Title
        /// </summary>
        [DataMember(Name = "parttitle")]
        public string PartTitle { get; set; }
        /// <summary>
        ///Done
        /// </summary>
        [DataMember(Name = "done")]
        public bool Done { get; set; }
        /// <summary>
        ///Part Title
        /// </summary>
        [DataMember(Name = "imageurl")]
        public string ImageUrl { get; set; }
        /// <summary>
        ///Partial Done
        /// </summary>
        [DataMember(Name = "partialdone")]
        public bool PartialDone { get; set; }
        /// <summary>
        /// Paid Status
        /// </summary>
        [DataMember(Name = "ispaid")]
        public bool IsPaid { get; set; }

        /// <summary>
        /// Module list
        /// </summary>
        [DataMember(Name = "modules")]
        public List<ChapterPartModuleInternal> Modules;

        /// <summary>
        /// Chapter is readable
        /// </summary>
        [DataMember(Name = "isReadable")]

        public bool isReadable = false;
        #endregion
    }
    public class ChapterPartInternal
    {
        #region Properties
        public int Id { get; set; }
        public int ChapterId { get; set; }
        public string Title_enUS { get; set; }
        public string Title_svSE { get; set; }
        public string PartTitle_enUS { get; set; }
        public string PartTitle_svSE { get; set; }
        public string ImageUrl { get; set; }
        public bool Done { get; set; }
        public bool PartialDone { get; set; }
        public bool IsPaid { get; set; }
        public List<ChapterPartModuleInternal> Modules;
        public bool isReadable = false;
        #endregion
    }
    [DataContract]
    public class ChapterPartAdd : ChapterPart
    {
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }

        [DataMember(Name = "title_svse")]
        public string Title_svSE { get; set; }

        [DataMember(Name = "parttitle_enus")]
        public string PartTitle_enUS { get; set; }

        [DataMember(Name = "parttitle_svse")]
        public string PartTitle_svSE { get; set; }

        public IFormFile Image { get; set; }
    }
    [DataContract]
    public class ChapterPartUpdate : ChapterPart
    {
        [DataMember(Name = "title_enus")]
        public string Title_enUS { get; set; }

        [DataMember(Name = "title_svse")]
        public string Title_svSE { get; set; }

        [DataMember(Name = "parttitle_enus")]
        public string PartTitle_enUS { get; set; }

        [DataMember(Name = "parttitle_svse")]
        public string PartTitle_svSE { get; set; }
        public IFormFile Image { get; set; }

    }
}
