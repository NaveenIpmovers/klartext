﻿using Microsoft.Extensions.Options;

namespace OtrohetsakutenApi.Models
{
    public interface IConfig
    {
        #region Properties
        AppSettings Settings { get; set; }
        #endregion
    }

    public class Config : IConfig
    {
        #region Properties
        public AppSettings Settings { get; set; }
        #endregion

        #region Constructor
        public Config(IOptions<AppSettings> AppSettings)
        {
            Settings = AppSettings.Value;
        }
        #endregion
    }
}